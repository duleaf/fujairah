//
//  Artical.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "Artical.h"


@implementation Artical

@dynamic art_id;
@dynamic articalType;
@dynamic date;
@dynamic details;
@dynamic imageUrl;
@dynamic title;
@dynamic details_ar;
@dynamic title_ar;

@end
