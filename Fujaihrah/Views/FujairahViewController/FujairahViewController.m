//
//  FujairahViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٨‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujairahViewController.h"

@interface FujairahViewController ()
{
    UIActivityIndicatorView * loading;
}
@end

@implementation FujairahViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void) showLoading
{
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loading.center = self.view.center;
    
    [loading startAnimating];
    [self.view addSubview:loading];
}

-(void) hideLoading
{
    [loading startAnimating];
    [loading removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
