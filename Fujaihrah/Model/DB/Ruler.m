//
//  Ruler.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "Ruler.h"


@implementation Ruler

@dynamic text;
@dynamic text_ar;
@dynamic title;
@dynamic title_ar;

@end
