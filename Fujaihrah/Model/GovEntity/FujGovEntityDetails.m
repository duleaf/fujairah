//
//  GovEntityDetails.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٣‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujGovEntityDetails.h"
#import "GovEntity.h"
#import "Utitlities.h"
@implementation FujGovEntityDetails

-(int)numberOfAvailableRecords
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"GovEntity" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    return (int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
}


-(void)insertObjects:(NSArray *)objects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"GovEntity" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:GovEntity.class];
    
    
    for(FujGovEntityDetails * fujEntity in objects)
    {
        GovEntity * govEn = [NSEntityDescription insertNewObjectForEntityForName:@"GovEntity" inManagedObjectContext:self.managedObjectContext];
        
        for(NSString * property in allProperties)
        {
            
            [govEn setValue:[fujEntity valueForKey:property] forKey:property];
            
        }
        
        [self.managedObjectContext save:nil];
    }
    
}

-(NSMutableArray *)getAllEntities
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"GovEntity"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:GovEntity.class];
    NSMutableArray * entities = [[NSMutableArray alloc] init];
    
    for(GovEntity * govEn in objects)
    {
        FujGovEntityDetails * fujEntity = [[FujGovEntityDetails alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [fujEntity setValue:[govEn valueForKey:property] forKey:property];
            
        }
        
        [entities addObject:fujEntity];
    }
    
    return entities;
}

-(void)deleteAllObjects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"GovEntity"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];

    for(NSManagedObject * object in objects)
        [self.managedObjectContext deleteObject:object];
    
}



@end
