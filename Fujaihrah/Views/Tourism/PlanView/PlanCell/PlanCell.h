//
//  PlanCell.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlanCell : UITableViewCell


@property (nonatomic,strong) IBOutlet UILabel * title;
@property (nonatomic,strong) IBOutlet UILabel * date;


@end
