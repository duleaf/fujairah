//
//  AboutUsViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AboutUsViewController : UIViewController <MFMailComposeViewControllerDelegate>

@property (nonatomic,strong) IBOutlet UITableView * table;


@end
