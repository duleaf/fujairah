//
//  StaticDataUpdateController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١١‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "StaticDataUpdateController.h"
#import "FujHistory.h"
#import "FujRurler.h"
#import "FujAboutUAE.h"
@implementation StaticDataUpdateController



-(void)StartUpdating
{
    if ([self.requestUri isEqualToString:@"get_page?id=2"]) {
        self.fujObject = [[FujHistory alloc] init];
    }
    else if ([self.requestUri isEqualToString:@"get_page?id=77"])
    {
        self.fujObject = [[FujAboutUAE alloc] init];
    }
    else
    {
        self.fujObject = [[FujRurler alloc] init];
    }
    
    
    if([self.fujObject numberOfAvailableRecords] > MIN_NUMBER_OF_ITEMS)
    {
        if ([self isNewUpdateAvailable])
        {
            ;
        }
        [self.delegate UpdateFinished];
    }
    else
    {
        [self.fujObject deleteAllObjects];
        [self fetchAllData];
    }
    
}


-(BOOL) isNewUpdateAvailable
{
    return true;
}

-(void)updateCurrentDataBase
{
    
}

-(void)fetchAllData
{
    NSString * extraUri = @"";
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        extraUri = @"/?lang=ar";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@%@",BASE_URL,self.requestUri,extraUri] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray * places = [self ParssedObjects:responseObject];
        [self.fujObject insertObjects:places];
        [self.delegate UpdateFinished];
        //        id obj = [[NSClassFromString(@"class") alloc] init];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


-(NSArray*)ParssedObjects:(id)response
{
    NSDictionary * responseDic = (NSDictionary*) response;
    NSMutableArray * compeletHistory = [[NSMutableArray alloc] init];
    NSDictionary * post = [responseDic valueForKey:@"page"];
    
//    for(NSDictionary * post in responsePosts)
    {
        FujHistory * history = [[FujHistory alloc] init];
        
        history.title = [post valueForKey:@"title"];
        history.title_ar = [post valueForKey:@"title_ar"];
//        NSNumber * number = [post valueForKey:@"id"];
//        place.p_id = [NSString stringWithFormat:@"%i",number.intValue];
        history.text = [post valueForKey:@"content"];
        history.text_ar = [post valueForKey:@"content_ar"];
        
        [compeletHistory addObject:history];
        
    }
    
    return compeletHistory;
}



@end
