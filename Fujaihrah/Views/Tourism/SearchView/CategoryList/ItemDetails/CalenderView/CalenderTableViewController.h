//
//  CalenderTableViewController.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FujPlan.h"
#import "FujPlace.h"
@protocol getDate <NSObject>

-(void)finish;

@end

@interface CalenderTableViewController : UITableViewController

@property (nonatomic,strong) IBOutlet UITextField * eventTitle;
@property (nonatomic,strong) IBOutlet UILabel * startTime;
@property (nonatomic,strong) IBOutlet UILabel * endTime;
@property (nonatomic,strong) IBOutlet UIDatePicker * startPicker;
@property (nonatomic,strong) IBOutlet UIDatePicker * endPicker;
@property (nonatomic,strong) FujPlan  * plan;
@property (nonatomic,strong) FujPlace  * place;



@end
