
//
//  GovernmentUpdateController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٩‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "GovernmentUpdateController.h"
#import "FujGovEntityDetails.h"
@implementation GovernmentUpdateController

-(void)StartUpdating
{
    self.fujObject = [[FujGovEntityDetails alloc] init];
    
    
    if([self.fujObject numberOfAvailableRecords] > MIN_NUMBER_OF_ITEMS)
    {
        if ([self isNewUpdateAvailable])
        {
            ;
        }
        [self.delegate UpdateFinished];
    }
    else
    {
        [self.fujObject deleteAllObjects];
        [self fetchAllData];
    }
    
}


-(BOOL) isNewUpdateAvailable
{
    return true;
}

-(void)updateCurrentDataBase
{
    
}

-(void)fetchAllData
{
    NSString * extraUri = @"";
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        extraUri = @"/?lang=ar";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@%@",BASE_URL,self.requestUri,extraUri] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray * entities = [self ParssedObjects:responseObject];
        [self.fujObject insertObjects:entities];
        [self.delegate UpdateFinished];
        //        id obj = [[NSClassFromString(@"class") alloc] init];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


-(NSArray*)ParssedObjects:(id)response
{
    NSDictionary * responseDic = (NSDictionary*) response;
    NSMutableArray * cats = [[NSMutableArray alloc] init];
    NSArray * responsePosts = [responseDic valueForKey:@"governments"];
    
    for(NSDictionary * post in responsePosts)
    {
        FujGovEntityDetails * entity = [[FujGovEntityDetails alloc] init];
        
        entity.name = [post valueForKey:@"title"];
        entity.name_ar = [post valueForKey:@"title_ar"];
        NSNumber * number = [post valueForKey:@"id"];
        entity.ent_id = [NSString stringWithFormat:@"%i",number.intValue];
        entity.intro = [post valueForKey:@"description"];
        entity.intro_ar = [post valueForKey:@"description_ar"];
        entity.lat = [post valueForKey:@"lat"];
        entity.lang = [post valueForKey:@"long"];
        entity.logo = [NSString stringWithFormat:@"%@",[post valueForKey:@"logo"]];
        entity.image = [NSString stringWithFormat:@"%@",[post valueForKey:@"cover_image"]];
        entity.fb = [post valueForKey:@"facebook"];
        entity.tw = [post valueForKey:@"twitter"];
        entity.webSite = [post valueForKey:@"website"];
        entity.phone = [post valueForKey:@"phone"];
        
        [cats addObject:entity];
        
    }
    
    return cats;
}

@end
