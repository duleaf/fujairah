//
//  CatsAndPlaces.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface CatsAndPlaces : NSManagedObject

@property (nonatomic, retain) NSString * cat_id;
@property (nonatomic, retain) NSString * p_id;

@end
