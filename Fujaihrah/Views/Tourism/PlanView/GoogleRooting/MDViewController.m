//
//  MDViewController.m
//  MapsDirections
//
//  Created by Mano Marks on 4/8/13.
//  Copyright (c) 2013 Google. All rights reserved.
//

#import "MDViewController.h"
#import "MDDirectionService.h"

@interface MDViewController () {
  GMSMapView *mapView_;
  NSMutableArray *waypoints_;
  NSMutableArray *waypointStrings_;
  CLLocationManager *locationManager;
    BOOL currentLocation;
    BOOL firstLocationUpdate_;
}
@end

@implementation MDViewController

CLLocationCoordinate2D from,to;
- (void)loadView {
    
    to.latitude = [self.place.lat doubleValue];
    to.longitude =  [self.place.lang doubleValue];
    currentLocation = false;
//    to.latitude = 25.173216;
//    to.longitude = 55.218265;
    
  waypoints_ = [[NSMutableArray alloc]init];
  waypointStrings_ = [[NSMutableArray alloc]init];
  GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:to.latitude
                                                          longitude:to.longitude
                                                               zoom:10];
  mapView_ = [GMSMapView mapWithFrame:CGRectZero camera:camera];
  mapView_.delegate = self;
  self.view = mapView_;
    [self getLocation];
    mapView_.settings.compassButton = YES;
    mapView_.settings.myLocationButton = YES;
    
    self.view = mapView_;

    
    if([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
           UIAlertView* alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                              delegate:nil
                                     cancelButtonTitle:@"OK"
                                     otherButtonTitles:nil];
            [alert show];
        }
    }

}

-(void) getLocation{
    locationManager = [[CLLocationManager alloc] init ];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    [locationManager requestWhenInUseAuthorization];

     [locationManager startUpdatingLocation];
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
    
    if (!currentLocation) {
        from.latitude = location.coordinate.latitude;
        from.longitude = location.coordinate.longitude;
        
        [self BuildRootFor:from];
        
        [self BuildRootFor:to];
        currentLocation = true;
    }
}

-(void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error{
    
    NSLog(@"%@",error.userInfo);
    if([CLLocationManager locationServicesEnabled]){
        
        NSLog(@"Location Services Enabled");
        
        if([CLLocationManager authorizationStatus]==kCLAuthorizationStatusDenied){
            UIAlertView    *alert = [[UIAlertView alloc] initWithTitle:@"App Permission Denied"
                                                               message:@"To re-enable, please go to Settings and turn on Location Service for this app."
                                                              delegate:nil
                                                     cancelButtonTitle:@"OK"
                                                     otherButtonTitles:nil];
            [alert show];
        }
    }
}


- (void)BuildRootFor:(CLLocationCoordinate2D)coordinate {
  
  CLLocationCoordinate2D position = CLLocationCoordinate2DMake(
                                    coordinate.latitude,
                                    coordinate.longitude);
  GMSMarker *marker = [GMSMarker markerWithPosition:position];
  marker.map = mapView_;
  [waypoints_ addObject:marker];
  NSString *positionString = [[NSString alloc] initWithFormat:@"%f,%f",
                              coordinate.latitude,coordinate.longitude];
    [waypointStrings_ addObject:positionString];
  if([waypoints_ count]>1){
    NSString *sensor = @"false";
    NSArray *parameters = [NSArray arrayWithObjects:sensor, waypointStrings_,
                           nil];
    NSArray *keys = [NSArray arrayWithObjects:@"sensor", @"waypoints", nil];
    NSDictionary *query = [NSDictionary dictionaryWithObjects:parameters
                                                      forKeys:keys];
    MDDirectionService *mds=[[MDDirectionService alloc] init];
    SEL selector = @selector(addDirections:);
    [mds setDirectionsQuery:query
               withSelector:selector
               withDelegate:self];
  }
}
- (void)addDirections:(NSDictionary *)json {

    NSArray * jsonObjects = [json objectForKey:@"routes"];
    
    if(jsonObjects.count < 1)
    {
        [[[UIAlertView alloc] initWithTitle:@"Directions not available" message:@"A rout to destination from its nearest road cannot be determined" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
        return;
    }
    
  NSDictionary *routes = [json objectForKey:@"routes"][0];
  
  NSDictionary *route = [routes objectForKey:@"overview_polyline"];
  NSString *overview_route = [route objectForKey:@"points"];
  GMSPath *path = [GMSPath pathFromEncodedPath:overview_route];
  GMSPolyline *polyline = [GMSPolyline polylineWithPath:path];
  polyline.map = mapView_;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
