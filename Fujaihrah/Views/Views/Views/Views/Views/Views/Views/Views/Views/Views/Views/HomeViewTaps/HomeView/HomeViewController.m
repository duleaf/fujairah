//
//  HomeViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeCell.h"
@interface HomeViewController ()

@end

@implementation HomeViewController
@synthesize infoArray;

//home , government ,history , about

int cellIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray*temArray1=[[NSArray alloc] initWithObjects:@"pic_1.png",@"bg_heading_details.png", nil];
    NSArray*temArray2=[[NSArray alloc] initWithObjects:@"pic_2.png",@"bg_heading_details_left.png", nil];
    NSArray*temArray3=[[NSArray alloc] initWithObjects:@"pic_3.png",@"bg_heading_details.png", nil];
    infoArray=[[NSMutableArray alloc] initWithObjects:temArray1,temArray2,temArray3,temArray2,temArray1, nil];
    currentCells = [[NSMutableArray alloc] init];
    cellIndex = 30;
    self.navigationController.navigationBarHidden = NO;
    UIViewController * viewc = [((UINavigationController*)self.sidePanelController.rightPanel).viewControllers objectAtIndex:0];
    
    [_barButton setTarget:viewc];
    [_barButton setAction:@selector(menuButtonPressed:)];
	// Do any additional setup after loading the view.
}


#pragma mark UITableViewDelegate / UITableViewDatasource Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 152.0;
    
}
-(void)viewDidAppear:(BOOL)animated
{
    for (int i = 0; i < currentCells.count; i++)
    {
       HomeCell* cell = (HomeCell*) [currentCells objectAtIndex:i];
        UIView *imageView = [cell viewWithTag:10];
        
        UIView *labelView = [cell viewWithTag:20];
        
        if(i %2 == 0)
        {
            cell.isAnimated = YES;
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 imageView.frame = CGRectMake( imageView.frame.origin.x - 165, imageView.frame.origin.y, imageView.frame.size.width , imageView.frame.size.height);
                                 
//                                 labelView.frame = CGRectMake( labelView.frame.origin.x + 155, labelView.frame.origin.y, labelView.frame.size.width , labelView.frame.size.height);
                             }
                             completion:nil];
        }
        else{
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 imageView.frame = CGRectMake( imageView.frame.origin.x + 160, imageView.frame.origin.y, imageView.frame.size.width , imageView.frame.size.height);
                                 
                                 labelView.frame = CGRectMake( 0, labelView.frame.origin.y, labelView.frame.size.width , labelView.frame.size.height);
                             }
                             completion:nil];
    
        }
    }
    cellIndex =  (int)currentCells.count -1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    // return [[[self fetchedResultsController]fetchedObjects]count];
    
    
    //Acordint to section approches
    //
    //	NSArray *sections = [[self fetchedResultsController] sections];
    //	if (tableView == self.searchDisplayController.searchResultsTableView)
    //	{
    //        return [self.searchResults count];
    //    }else if (sectionIndex < [sections count])
    //	{
    //		id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:sectionIndex];
    //		return sectionInfo.numberOfObjects;
    //	}
	
	return 3;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    HomeCell *cell = nil;

    if(indexPath.row < currentCells.count)
    cell = [currentCells objectAtIndex:indexPath.row];
    
    UIView *imageView = [cell viewWithTag:10];
    
    UIView *labelView = [cell viewWithTag:20];
    
    if (cell  == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:nil options:nil] firstObject] ;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        imageView = [[[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:nil options:nil] objectAtIndex:1];
        
        labelView = [[[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:nil options:nil] objectAtIndex:2];
        cell.isAnimated = false;
        
        [cell addSubview:labelView];
        [cell addSubview:imageView];
        
        UIImageView * firstImg = (UIImageView *)[imageView viewWithTag:1];
        
        UIImageView * secImg = (UIImageView *)[imageView viewWithTag:2];
        
        UIImageView * thirdImg = (UIImageView *)[labelView viewWithTag:3];
        
        
        firstImg.image=[UIImage imageNamed:[[infoArray objectAtIndex:indexPath.row] objectAtIndex:0]];
        secImg.image=[UIImage imageNamed:[[infoArray objectAtIndex:indexPath.row] objectAtIndex:0]];
        
        thirdImg.image = [UIImage imageNamed:[[infoArray objectAtIndex:indexPath.row] objectAtIndex:1]];
        
        thirdImg.backgroundColor = [UIColor brownColor];
        
        [currentCells addObject:cell];
        
    }
    

    if((!cell.isAnimated) && cellIndex < indexPath.row)
    {
        if(indexPath.row %2 == 0)
        {
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 imageView.frame = CGRectMake( imageView.frame.origin.x - 165, imageView.frame.origin.y, imageView.frame.size.width , imageView.frame.size.height);
                                 
//                                 labelView.frame = CGRectMake( labelView.frame.origin.x + 155, labelView.frame.origin.y, labelView.frame.size.width , labelView.frame.size.height);
                             }
                             completion:nil];
        }
        else{
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 imageView.frame = CGRectMake( imageView.frame.origin.x + 160, imageView.frame.origin.y, imageView.frame.size.width , imageView.frame.size.height);
                                 
                                 labelView.frame = CGRectMake( -165, labelView.frame.origin.y, labelView.frame.size.width , labelView.frame.size.height);
                             }
                             completion:nil];
            
        }
        cell.isAnimated = true;

    }
    

    
    return cell;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
