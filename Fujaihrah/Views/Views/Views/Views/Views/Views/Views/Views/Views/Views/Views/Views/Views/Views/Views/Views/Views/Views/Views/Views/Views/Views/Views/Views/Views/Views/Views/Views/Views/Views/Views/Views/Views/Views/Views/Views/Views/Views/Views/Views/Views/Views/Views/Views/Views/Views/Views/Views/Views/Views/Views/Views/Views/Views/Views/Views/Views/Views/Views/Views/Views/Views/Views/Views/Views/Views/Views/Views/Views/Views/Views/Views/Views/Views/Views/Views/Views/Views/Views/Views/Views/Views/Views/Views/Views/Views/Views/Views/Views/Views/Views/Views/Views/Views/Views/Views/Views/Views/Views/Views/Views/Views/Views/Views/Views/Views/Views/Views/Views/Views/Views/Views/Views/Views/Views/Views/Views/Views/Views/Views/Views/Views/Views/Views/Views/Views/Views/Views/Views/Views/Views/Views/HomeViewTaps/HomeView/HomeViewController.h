//
//  HomeViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HomeViewController : UIViewController
{
    IBOutlet UITableView *tableView;
    NSMutableArray * currentCells;
}

@property (nonatomic,strong) IBOutlet UIBarButtonItem * barButton;

@property (nonatomic,strong) NSMutableArray * infoArray;

@end
