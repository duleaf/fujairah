//
//  GuideTabViewController.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٤‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "GuideTabViewController.h"
#import "SearchView/SearchViewController.h"
@interface GuideTabViewController ()
{
    SearchViewController *listViewController1;
    UITableViewController *listViewController2 ;
}
@end

@implementation GuideTabViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    
     listViewController1= [self.storyboard instantiateViewControllerWithIdentifier:@"search"];
	 listViewController2= [self.storyboard instantiateViewControllerWithIdentifier:@"plan"];
	
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
	self.viewControllers = @[listViewController1, listViewController2];
    
    [super viewDidLoad];


    
}

-(void)viewWillAppear:(BOOL)animated
{
    self.title = [LanguageController localizedStringForKey:@"guide"];
    
    listViewController1.title = [LanguageController localizedStringForKey:@"search"];;
	listViewController2.title = [LanguageController localizedStringForKey:@"plan"];;
    [self updateTabLanguages];
    
    [listViewController1 viewWillAppear:YES];
    [listViewController2 viewWillAppear:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)addTabButtons
{
	NSUInteger index = 0;
	for (UIViewController *viewController in self.viewControllers)
	{
		UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
		button.tag = 1000 + index;
        button.backgroundColor = [UIColor groupTableViewBackgroundColor];
		button.titleLabel.font = [UIFont boldSystemFontOfSize:18];
		button.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        
        
		UIOffset offset = viewController.tabBarItem.titlePositionAdjustment;
		button.titleEdgeInsets = UIEdgeInsetsMake(offset.vertical, offset.horizontal, 0.0f, 0.0f);
		button.imageEdgeInsets = viewController.tabBarItem.imageInsets;
		[button setTitle:viewController.tabBarItem.title forState:UIControlStateNormal];
        
        
		[button addTarget:self action:@selector(tabButtonPressed:) forControlEvents:UIControlEventTouchDown];
        
//        UIImageView *image = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"plan_selector.png"]];
//        [button setBackgroundImage:image.image forState:UIControlStateNormal];
        
		[self deselectTabButton:button];
		[super.tabButtonsContainerView addSubview:button];
        
		++index;
	}
}


#pragma mark - Change these methods to customize the look of the buttons

- (void)selectTabButton:(UIButton *)button
{
	[button setTitleColor:[UIColor redColor] forState:UIControlStateNormal];
    
	UIImage *image = [[UIImage imageNamed:@"plan_selector.png"] stretchableImageWithLeftCapWidth:0 topCapHeight:0];
	[button setBackgroundImage:image forState:UIControlStateNormal];
	[button setBackgroundImage:image forState:UIControlStateHighlighted];
	
    
	[button setTitleColor:[UIColor colorWithRed:255.0/255.0f green:0.0f blue:0.0f alpha:1.0f] forState:UIControlStateNormal];
    //	[button setTitleShadowColor:[UIColor colorWithWhite:0.0f alpha:0.5f] forState:UIControlStateNormal];
}

- (void)deselectTabButton:(UIButton *)button
{
	[button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    
	UIImage *image = [[UIImage imageNamed:@"unselected.png"] stretchableImageWithLeftCapWidth:1 topCapHeight:0];
	[button setBackgroundImage:image forState:UIControlStateNormal];
	[button setBackgroundImage:image forState:UIControlStateHighlighted];
    
	[button setTitleColor:[UIColor colorWithRed:255.0/255.0f green:0.0f blue:0.0f alpha:1.0f]forState:UIControlStateNormal];
    //	[button setTitleShadowColor:[UIColor redColor] forState:UIControlStateNormal];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
