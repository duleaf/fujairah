//
//  History.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface History : NSManagedObject

@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * text_ar;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * title_ar;

@end
