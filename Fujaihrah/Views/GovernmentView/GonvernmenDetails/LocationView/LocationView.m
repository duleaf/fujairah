//
//  LocationView.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "LocationView.h"
#import "VBAnnotation.h"


#define RE_LATITUDE 36.7478
#define RE_LONGITUDE -119.7714

@interface LocationView ()
{
    MKDirectionsRequest *request ;
}
@end

@implementation LocationView


@synthesize iMapView;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

-(void)awakeFromNib
{

}


-(void)drawLocations
{
    MKCoordinateRegion region;
    region.center.latitude = RE_LATITUDE;
    region.center.longitude = RE_LONGITUDE;
    region.span.latitudeDelta = 2;
    region.span.longitudeDelta = 2;
    [iMapView setRegion:region animated:NO];
    
    // create annotations
    NSMutableArray *annotations = [[NSMutableArray alloc] init];
    CLLocationCoordinate2D location;
    VBAnnotation *annotation;
    
    location = [self getLocation];
    
    for(id object in _locations)
    {
        location.latitude = [(NSString*)[object valueForKey:@"lat"] doubleValue];
        location.longitude = [(NSString*)[object valueForKey:@"lang"] doubleValue];
        
        annotation = [[VBAnnotation alloc] init];
        [annotation setCoordinate:location];
        annotation.title = (NSString*)[object valueForKey:@"name"];
        [annotations addObject:annotation];
    }
    
    [iMapView addAnnotations:annotations];
    
    [iMapView showAnnotations:annotations animated:YES];
    
    iMapView.camera.altitude *= 1.4;
}

-(CLLocationCoordinate2D) getLocation{
    CLLocationManager *locationManager = [[CLLocationManager alloc] init ];
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    
    [locationManager startUpdatingLocation];
    [locationManager requestAlwaysAuthorization];
    
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    // [locationManager startUpdatingLocation];
    return coordinate;
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations {
    CLLocation *location = [locations lastObject];
    NSLog(@"lat%f - lon%f", location.coordinate.latitude, location.coordinate.longitude);
}
- (MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    static NSString *identifier = @"pin";
    MKAnnotationView *view = [mapView dequeueReusableAnnotationViewWithIdentifier:identifier];
    if (view == nil) {
        view = [[MKPinAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:identifier];
    }
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sun.png"]];
    view.leftCalloutAccessoryView = imageView;
    
    view.enabled = YES;
    view.canShowCallout = YES;
    return view;
}


-(void) drawRootForm:(CLLocationCoordinate2D)source ToDestination:(CLLocationCoordinate2D)dest
{
    request = [[MKDirectionsRequest alloc] init];
    self.iMapView.delegate = self;
    
    MKCoordinateRegion region;
    region.center = source;
    region.span.latitudeDelta = 2;
    region.span.longitudeDelta = 2;
    [iMapView setRegion:region animated:YES];
    
    MKPlacemark *placeMaker = [[MKPlacemark alloc] initWithCoordinate:source addressDictionary:nil];
    MKMapItem * item = [[MKMapItem alloc] initWithPlacemark:placeMaker];

    request.source = item;
    
    placeMaker = [[MKPlacemark alloc] initWithCoordinate:dest addressDictionary:nil];
    item = [[MKMapItem alloc] initWithPlacemark:placeMaker];

    request.destination = item;
    request.requestsAlternateRoutes = YES;
    MKDirections *directions =
    [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             // Handle Error
              NSLog(@"ERROR: %@",error);
             [[[UIAlertView alloc] initWithTitle:@"Directions" message:error.localizedFailureReason delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
         } else {
             [self showRoute:response];
         }
     }];
}



-(void) drawRootFormMyLocationToDestination:(CLLocationCoordinate2D)dest
{
    request = [[MKDirectionsRequest alloc] init];
    MKPlacemark *placeMaker;
    MKMapItem * item;
    self.iMapView.delegate = self;
    MKCoordinateRegion region;
    region.center = dest;
    region.span.latitudeDelta = 7;
    region.span.longitudeDelta = 7;
    [iMapView setRegion:region animated:YES];
    
    request.source = [MKMapItem mapItemForCurrentLocation];
    
    placeMaker = [[MKPlacemark alloc] initWithCoordinate:dest addressDictionary:nil];
    item = [[MKMapItem alloc] initWithPlacemark:placeMaker];
    
    request.destination = item;
    request.requestsAlternateRoutes = YES;
    MKDirections *directions =
    [[MKDirections alloc] initWithRequest:request];
    
    [directions calculateDirectionsWithCompletionHandler:
     ^(MKDirectionsResponse *response, NSError *error) {
         if (error) {
             // Handle Error
             NSLog(@"ERROR: %@",error);
             [[[UIAlertView alloc] initWithTitle:@"Directions" message:@"Sorry, no possible roots to show" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil] show];
         } else {
             [self showRoute:response];
         }
     }];
}

-(void)showRoute:(MKDirectionsResponse *)response
{
    for (MKRoute *route in response.routes)
    {
        [iMapView
         addOverlay:route.polyline level:MKOverlayLevelAboveRoads];
        
        for (MKRouteStep *step in route.steps)
        {
            NSLog(@"%@", step.instructions);
        }
    }
}

- (MKOverlayRenderer *)mapView:(MKMapView *)mapView rendererForOverlay:(id < MKOverlay >)overlay
{
    MKPolylineRenderer *renderer =
    [[MKPolylineRenderer alloc] initWithOverlay:overlay];
    renderer.strokeColor = [UIColor blueColor];
    renderer.lineWidth = 5.0;
    return renderer;
}



@end
