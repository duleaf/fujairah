//
//  FujairahObject.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
@interface FujairahObject : NSObject

@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;


-(int)numberOfAvailableRecords;
-(void) insertObjects:(NSArray*)objects;
-(void)deleteAllObjects;
@end
