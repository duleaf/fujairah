//
//  MDViewController.h
//  MapsDirections
//
//  Created by Mano Marks on 4/8/13.
//  Copyright (c) 2013 Google. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import "FujPlace.h"
#import <CoreLocation/CoreLocation.h>
@interface MDViewController : UIViewController <GMSMapViewDelegate,CLLocationManagerDelegate>

@property (nonatomic,strong) FujPlace * place;
@end
