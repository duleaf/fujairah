//
//  Artical.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Artical : NSManagedObject

@property (nonatomic, retain) NSString * art_id;
@property (nonatomic, retain) NSString * articalType;
@property (nonatomic, retain) NSString * date;
@property (nonatomic, retain) NSString * details;
@property (nonatomic, retain) NSString * imageUrl;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * details_ar;
@property (nonatomic, retain) NSString * title_ar;

@end
