//
//  CatsAndPlaces.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١١‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujairahObject.h"
#import "PlacesCateogries.h"
#import "Place.h"
#import "CatsAndPlaces.h"
@interface FujCatsAndPlaces : FujairahObject

@property (nonatomic, retain) NSString * cat_id;
@property (nonatomic, retain) NSString * p_id;

-(NSMutableArray*)getAllPlacesIdsFromCategory:(PlacesCateogries*)cat;

@end
