//
//  ViewController.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٤‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "SearchViewController.h"
#import "CategoryList/CategoryListViewController.h"
#import "FujPlacesCateogries.h"
#import "ItemDetailsViewController.h"
@interface SearchViewController ()
{
    FujPlacesCateogries * cats;
    NSArray * titles;
    BOOL searchMode;
}
@end

@implementation SearchViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    searchMode = false;
    cats = [[FujPlacesCateogries alloc] init];
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(reloadCurrenData:)
     name:NSStringFromClass(self.class)
     object:nil];
    
    titles= [cats getAllCategories];
    
    if(titles.count == 0)
    {
        self.tableView.hidden = YES;
        [self showLoading];
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self.tableView reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return titles.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *cellId = [NSString stringWithFormat:@"cell%i",(int)indexPath.row];
    
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];
        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
        }
    
    UILabel * title = (UILabel*) [cell viewWithTag:1];
    
    
    
    
    
    
    if(searchMode)
    {
        FujPlace *place = [titles objectAtIndex:indexPath.row];
        title.text = place.name;
    }
    else
    {
        
        cats = [titles objectAtIndex:indexPath.row];
        title.text = cats.name;
    }
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
    {
        title.textAlignment = NSTextAlignmentRight;
        title.text = cats.name_ar;
        
        if(searchMode)
        {
            FujPlace *place = [titles objectAtIndex:indexPath.row];
            title.text = place.name_ar;
        }
     }
    
        return cell;
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(searchMode)
    {
        NSString * nibName = @"ItemDetailsViewController";
        
        if ([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        {
            nibName = @"ItemDetailsViewController_ar";
        }
        
        ItemDetailsViewController *itemDetails = [[ItemDetailsViewController alloc] initWithNibName:nibName bundle:[NSBundle mainBundle]];
        
        itemDetails.place = [titles objectAtIndex:indexPath.row];
        
        [self.navigationController pushViewController:itemDetails animated:YES];
    }
    else
    {
        CategoryListViewController  *catList = [self.storyboard instantiateViewControllerWithIdentifier:@"categories"];
        
        [self.navigationController pushViewController:catList animated:YES];
    }
    
    
}

-(void)reloadCurrenData:(NSNotification*)userInfo
{
    titles= [cats getAllCategories];
    [self hideLoading];
    self.tableView.hidden = NO;
    [self.tableView reloadData];
    
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    NSString * fullString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    NSLog(@"TEXT: %@",fullString);
    if(fullString.length == 1 && string.length == 0)
    {
        searchMode = false;
        [self reloadCurrenData:nil];
        textField.text = @"";
        [textField resignFirstResponder];
    }
    else
    {
        searchMode = true;
        FujPlace *place = [[FujPlace alloc] init];
        titles = [place getAllCategoriesConatinsString:fullString];
        [self.tableView reloadData];
    }
    
    
    return true;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return true;
}
- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
