//
//  GonvernmenDetailsViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٥‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "GonvernmenDetailsViewController.h"
#import "LocationView.h"
#import "Utitlities.h"
@interface GonvernmenDetailsViewController ()

@end

@implementation GonvernmenDetailsViewController

int height;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    height = 200;
    self.table.backgroundView = [[UIView alloc] initWithFrame:self.table.frame];
    self.table.backgroundColor = [UIColor clearColor];
    
    UIImageView * backgrounImage = [[UIImageView alloc] initWithFrame:self.table.frame];
    [backgrounImage setImage:[UIImage imageNamed:@"baladya_bg.png"]];
    [self.table.backgroundView addSubview:backgrounImage];
    
    self.title = self.entity.name;
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        self.title = self.entity.name_ar ;

    // Do any additional setup after loading the view.
}



- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    if(self.entity.targets.length < 1)
        return 1;
    else
        return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    int viewIndex = (int)indexPath.row * 2;
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
    {
        viewIndex ++;
    }
    
    
    if (cell  == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"GonvernmenDetails" owner:nil options:nil] objectAtIndex:viewIndex] ;
        
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [self handelCell:cell forIndex:indexPath];
    
//    cell.img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.jpg",(int)indexPath.row % 3]];
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
    
}

-(void)handelCell:(UITableViewCell *)cell forIndex:(NSIndexPath * )indexPath
{
    if (indexPath.row == 0)
    {
        UIImageView * coverImage = (UIImageView *) [cell viewWithTag:1];
        UIImageView * logo = (UIImageView *)[cell viewWithTag:2];
        
        [logo.layer setCornerRadius:4];
        logo.clipsToBounds = YES;
        
        self.webView =  (UIWebView *) [cell viewWithTag:4];
        self.webView.backgroundColor = [UIColor clearColor];
        self.webView.delegate = self;
        [self loadWebView];
        [coverImage sd_setImageWithURL:[NSURL URLWithString:self.entity.image] placeholderImage:nil];
        [logo sd_setImageWithURL:[NSURL URLWithString:self.entity.logo] placeholderImage:nil];
        
    }
    
    
    else if (indexPath.row == 1)
    {
        UILabel * details =  (UILabel *) [cell viewWithTag:2];
       
        details.text = self.entity.targets;
        details.frame = [Utitlities getLableSizeFromText:self.entity.targets andLable:details];
    }
    
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0)
    {
        int startPoint = 232;
        
        return startPoint + height ;
    }
    else
    {
        int startPoint = 50;
        UILabel * goals = [[UILabel alloc] initWithFrame:CGRectMake(20, startPoint, 280, 50)];
        
        CGRect frame = [Utitlities getLableSizeFromText:self.entity.targets andLable:goals];
        
        return frame.origin.y + frame.size.height;
    }
    
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)callPhone:(id)sender
{
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",@"0097192227000"]]];
    } else {
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
}

-(IBAction)openLink:(id)sender
{
    NSString * url ;
    UIButton * button = (UIButton*)sender;
    
    if (button.tag == 1)
    {
        url = @"https://www.facebook.com/FujairahMunicipality";
        NSURL * nsurl = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",@"131389563559783"]];
        
        if ([[UIApplication sharedApplication] canOpenURL:nsurl]){
            [[UIApplication sharedApplication] openURL:nsurl];
        }
        else {
            NSURL *link = [NSURL URLWithString:url];
            [[UIApplication sharedApplication] openURL:link];
        }
    }
    
    else if (button.tag == 2)
    {
        url = self.entity.tw;
        
        if(self.entity.tw.length < 1)
            return;
        
        NSURL * nsurl = [NSURL URLWithString:[NSString stringWithFormat:@"twitter://user?screen_name=%@",[url lastPathComponent]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:nsurl]){
            [[UIApplication sharedApplication] openURL:nsurl];
        }
        else {
            NSURL *link = [NSURL URLWithString:url];
            [[UIApplication sharedApplication] openURL:link];
        }
    }
    else
    {
        url = @"http://www.fujmun.gov.ae/";
        
        NSURL *link = [NSURL URLWithString:url];
        [[UIApplication sharedApplication] openURL:link];
        
    }
}

-(IBAction)showMap:(id)sender
{
    UIViewController * mapView = [[UIViewController alloc] init];
    mapView.navigationController.navigationBar.tintColor = [UIColor whiteColor];

    LocationView*  locationView = [[[NSBundle mainBundle] loadNibNamed:@"LocationView" owner:nil options:nil] firstObject];
    
    locationView.frame = mapView.view.frame;
    locationView.iMapView.frame = mapView.view.frame;
    
    [mapView.view addSubview:locationView];
    
    mapView.title = @"Title";
    
    [self.navigationController pushViewController:mapView animated:YES];
    
    mapView.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    //    frame.size.height = 1;
    //    aWebView.frame = frame;
    BOOL flage = (height == 200);
    
    
    height = [[aWebView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue];
    
    if(flage)
    {
        self.webView.frame = CGRectMake(self.webView.frame.origin.x, self.webView.frame.origin.y, self.webView.frame.size.width, height);
        [self.table reloadData];
    }
}

-(void)loadWebView
{
    NSString * html = [Utitlities getHtmlFromatFor:self.entity.intro];
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        html = [Utitlities getHtmlFromatFor:self.entity.intro_ar] ;

    
    [self.webView loadHTMLString:html baseURL:nil];
    self.webView.frame = CGRectMake(self.webView.frame.origin.x, self.webView.frame.origin.y, self.webView.frame.size.width, height);
    self.webView.backgroundColor = [UIColor clearColor];
    [self.webView setOpaque:NO];
    NSLog(@"%f",self.webView.scrollView.contentSize.height);
    self.webView.scrollView.scrollEnabled = NO;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
