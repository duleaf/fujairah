//
//  AboutUAE.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "AboutUAE.h"


@implementation AboutUAE

@dynamic text;
@dynamic text_ar;
@dynamic title;
@dynamic title_ar;

@end
