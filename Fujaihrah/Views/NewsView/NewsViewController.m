//
//  NewsViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsTableViewCell.h"
#import "NewsHolderViewController.h"
#import "FujArtical.h"
#import "AppDelegate.h"
#define DefualtColor [UIColor colorWithRed:189.0/255.0f green:0.0f blue:0.0f alpha:1.0f]

@interface NewsViewController ()
{
    NewsHolderViewController * socialView;
    NewsHolderViewController * newsView;
    UIActivityIndicatorView * loading;

    
}
@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    socialView = [[NewsHolderViewController alloc] initWithStyle:UITableViewStylePlain];
    newsView = [[NewsHolderViewController alloc] initWithStyle:UITableViewStylePlain];
    
    
    
    socialView.parentView = self;
    newsView.parentView = self;
    socialView.type = @"social";
    newsView.type = @"normal";
    
    if([[LanguageController  getCurrentLanguage] isEqualToString:@"ar"])
    {
        self.title = @"اخبار الفجيرة";
    }
    else
    {
        self.title = @"Fujairah News";
    }
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(reloadCurrenData:)
     name:NSStringFromClass(self.class)
     object:nil];
    
    FujArtical *artical = [[FujArtical alloc] init];
    
    socialView.articals = [artical getAllSocialArticals];

    
    newsView.articals = [artical getAllArticals ];
    
    if(newsView.articals.count == 0 || socialView.articals.count == 0)
    {
        [self showLoading];
    }
    
    
    self.viewControllers = @[socialView,newsView];
    
    [super viewDidLoad];
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:189.0/255.0f green:0.0f blue:0.0f alpha:1.0f];
}

-(void)viewWillAppear:(BOOL)animated
{
//    self.title = [LanguageController localizedStringForKey:@"news"];
//
    NSLog(@"News Title: %@",[LanguageController localizedStringForKey:@"news"]);
    newsView.title = [LanguageController localizedStringForKey:@"news"];
	socialView.title = [LanguageController localizedStringForKey:@"social"];
//
    [self updateTabLanguages];
    
    [socialView viewWillAppear:YES];
    [newsView viewWillAppear:YES];
    
}

-(NSArray *) getNews
{
    NSMutableArray * socialNews = [[NSMutableArray alloc] init];
    
    FujArtical *artical = [[FujArtical alloc] init];
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
    {
        
        artical.title = @"إعتماد 208 طلبات أراض لأهالي إمارة الشارقة";
        artical.date = @"19 أكتوبر 2014";
        
        [socialNews addObject:artical];
        
        artical = [[FujArtical alloc] init];
        artical.title = @"محمد بن راشد يطلق الاستراتيجية الوطنية للابتكار";
        artical.date = @"19 أكتوبر 2014";
        
        [socialNews addObject:artical];
        
        
        artical = [[FujArtical alloc] init];
        artical.title = @"أشياء من الماضي";
        artical.date = @"19 أكتوبر 2014";
        
        [socialNews addObject:artical];
        artical = [[FujArtical alloc] init];
        artical.title = @"إعتماد 208 طلبات أراض لأهالي إمارة الشارقة";
        artical.date = @"19 أكتوبر 2014";
        
        [socialNews addObject:artical];
        
        artical = [[FujArtical alloc] init];
        artical.title = @"محمد بن راشد يطلق الاستراتيجية الوطنية للابتكار";
        artical.date = @"19 أكتوبر 2014";
        
        [socialNews addObject:artical];
        
        
        artical = [[FujArtical alloc] init];
        artical.title = @"أشياء من الماضي";
        artical.date = @"19 أكتوبر 2014";
        
        [socialNews addObject:artical];
    }
    else
    {
        
        artical.title = @"Umm Al Quwain CID arrest gang for e-mail fraud";
        artical.date = @"October 19, 2014";
        
        [socialNews addObject:artical];
        
        artical = [[FujArtical alloc] init];
        artical.title = @"Maid accused of forcing children to eat at knifepoint";
        artical.date = @"October 19, 2014";
        
        [socialNews addObject:artical];
        
        
        artical = [[FujArtical alloc] init];
        artical.title = @"Abu Dhabi Strategic Debate forum opens";
        artical.date = @"October 19, 2014";
        
        [socialNews addObject:artical];
        
         artical = [[FujArtical alloc] init];
        artical.title = @"Umm Al Quwain CID arrest gang for e-mail fraud";
        artical.date = @"October 19, 2014";
        
        [socialNews addObject:artical];
        
        artical = [[FujArtical alloc] init];
        artical.title = @"Maid accused of forcing children to eat at knifepoint";
        artical.date = @"October 19, 2014";
        
        [socialNews addObject:artical];
        
        
        artical = [[FujArtical alloc] init];
        artical.title = @"Abu Dhabi Strategic Debate forum opens";
        artical.date = @"October 19, 2014";
        
        [socialNews addObject:artical];
    }
    
    return [artical getAllArticals];
}



- (void)addTabButtons
{
	NSUInteger index = 0;
	for (UIViewController *viewController in self.viewControllers)
	{
		UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
		button.tag = 1000 + index;
		button.titleLabel.font = [UIFont boldSystemFontOfSize:15];
		button.titleLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
        button.layer.borderWidth=1.0f;
        button.layer.borderColor=[DefualtColor CGColor];
        button.titleLabel.textColor = [UIColor whiteColor];
        button.backgroundColor = [UIColor clearColor];
        [button.layer setCornerRadius:4];
        button.clipsToBounds = YES;
        
		int  offset = (int)index * 100;
		button.frame = CGRectMake(55 + (offset) ,10,110,30);
//		button.imageEdgeInsets = viewController.tabBarItem.imageInsets;
		[button setTitle:viewController.tabBarItem.title forState:UIControlStateNormal];
//		[button setImage:viewController.tabBarItem.image forState:UIControlStateNormal];
        
		[button addTarget:self action:@selector(tabButtonPressed:) forControlEvents:UIControlEventTouchDown];
        
		[self deselectTabButton:button];
		[super.tabButtonsContainerView addSubview:button];
        
        
		++index;
	}
}


- (void)selectTabButton:(UIButton *)button
{
	[button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    
    [button setBackgroundColor:DefualtColor];
    //	[button setTitleShadowColor:[UIColor colorWithWhite:0.0f alpha:0.5f] forState:UIControlStateNormal];
}

- (void)deselectTabButton:(UIButton *)button
{
	[button setTitleColor:DefualtColor forState:UIControlStateNormal];
    
    [button setBackgroundColor:[UIColor clearColor]];
    //	[button setTitleShadowColor:[UIColor redColor] forState:UIControlStateNormal];
}



- (void)layoutTabButtons
{
//	NSUInteger index = 0;
//	NSUInteger count = [self.viewControllers count];
//    
//	CGRect rect = CGRectMake(0.0f, 0.0f, floorf(self.view.bounds.size.width / count), );
//    
//	indicatorImageView.hidden = YES;
//    
//	NSArray *buttons = [tabButtonsContainerView subviews];
//	for (UIButton *button in buttons)
//	{
//		if (index == count - 1)
//			rect.size.width = self.view.bounds.size.width - rect.origin.x;
//        
//		button.frame = rect;
//		rect.origin.x += rect.size.width;
//        
//		if (index == self.selectedIndex)
//			[self centerIndicatorOnButton:button];
//        
//		++index;
//	}
}

-(void)reloadCurrenData:(NSNotification*)userInfo
{
    FujArtical *artical = [[FujArtical alloc] init];
    
    socialView.articals = [artical getAllSocialArticals];
    newsView.articals = [artical getAllArticals ];
    
    if((socialView.articals.count > 0) && (newsView.articals.count > 0))
    {
        [self hideLoading];

    }
    
    [socialView.tableView reloadData];
    [newsView.tableView reloadData];

}

-(void) showLoading
{
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loading.center = self.view.center;
    
    [loading startAnimating];
    [self.navigationController.view addSubview:loading];
    
 
    
}

-(void) hideLoading
{
    [loading stopAnimating];
    [loading removeFromSuperview];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

@end
