//
//  FujPlan.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujPlan.h"
#import "Utitlities.h"
#import "AppDelegate.h"

@implementation FujPlan

- (id)init
{
    self = [super init];
    
    if (self) {
        //1
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2
        self.managedObjectContext = appDelegate.managedObjectContext;
        
    }
    
    return self;
}

-(void)insertMeUp
{
    
    if (![self isPlanExistWithPlace:self.place])
    {
        Plan * plan = [NSEntityDescription insertNewObjectForEntityForName:@"Plan" inManagedObjectContext:self.managedObjectContext];
        plan.date = self.date;
    
        plan.place_id  = self.place.p_id;
        
    }
    else
    {
        NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Plan"];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"Plan" inManagedObjectContext:self.managedObjectContext];
        
        NSPredicate *predicate       = [NSPredicate predicateWithFormat:@"place_id==%@",self.place.p_id];
        
        [fetchRequest setEntity:entity];
        fetchRequest.predicate = predicate;
        
        Plan *plan = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] lastObject];
        plan.date = self.date;
        
    }
    
    
    NSError * error;
    [self.managedObjectContext save:&error];
}

-(BOOL)isPlanExistWithPlace:(FujPlace*)place
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Plan"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Plan" inManagedObjectContext:self.managedObjectContext];
    
    NSPredicate *predicate       = [NSPredicate predicateWithFormat:@"place_id==%@",place.p_id];
    
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = predicate;
    
    NSArray *objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    

    return (objects.count >= 1);
}

-(NSArray*) getListOFPlans
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Plan"];
    NSArray * list = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSMutableArray * plans = [[NSMutableArray alloc] init];
    
    for (Plan * plan in list)
    {
        if([plan.date compare:[NSDate date]] == NSOrderedAscending)
        {
            [self.managedObjectContext deleteObject:plan];
            continue;
        }
        
        FujPlan * Fplan = [[FujPlan alloc] init];
        FujPlace *Fplace = [[FujPlace alloc] init];
        
        Fplan.date = plan.date;
        Fplan.place = [Fplace getPlaceFromFujPlaceID:plan.place_id ];
        
        [plans addObject:Fplan];
    }
    
    
    return [NSArray arrayWithArray:plans];
}



@end
