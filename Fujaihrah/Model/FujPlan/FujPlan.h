//
//  FujPlan.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FujPlace.h"
#import "FujairahObject.h"
#import "Plan.h"
#import "Place.h"
@interface FujPlan : FujairahObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) FujPlace *place;
@property (nonatomic, retain) NSManagedObjectContext *managedObjectContext;

-(void)insertMeUp;
-(NSArray*) getListOFPlans;
-(BOOL)isPlanExistWithPlace:(FujPlace*)place;

@end
