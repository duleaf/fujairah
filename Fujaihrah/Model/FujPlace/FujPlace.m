//
//  FujPlace.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujPlace.h"
#import "Utitlities.h"
#import "Place.h"
@implementation FujPlace


-(int)numberOfAvailableRecords
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Place" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    return (int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
}


-(void)insertObjects:(NSArray *)objects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Place" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Place.class];
    
    
    for(FujPlace * fujPlace in objects)
    {
        Place * place = [NSEntityDescription insertNewObjectForEntityForName:@"Place" inManagedObjectContext:self.managedObjectContext];
        
        for(NSString * property in allProperties)
        {
            
            [place setValue:[fujPlace valueForKey:property] forKey:property];
            
        }
        
        [self.managedObjectContext save:nil];
    }
    
}

-(NSMutableArray *)getAllPlaces
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Place"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Place.class];
    NSMutableArray * entities = [[NSMutableArray alloc] init];
    
    for(Place * place in objects)
    {
        FujPlace * fujPlace = [[FujPlace alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [fujPlace setValue:[place valueForKey:property] forKey:property];
            
        }
        
        [entities addObject:fujPlace];
    }
    
    return entities;
}


-(NSArray *)getAllCategoriesConatinsString:(NSString*) str
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Place"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Place" inManagedObjectContext:self.managedObjectContext];
    
    NSPredicate *predicate       = [NSPredicate predicateWithFormat:@"name CONTAINS %@",str];
    
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = predicate;
    
    NSArray *objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Place.class];
    NSMutableArray * places = [[NSMutableArray alloc] init];
    FujPlace * fujPlace = [[FujPlace alloc] init];
    
    for(Place * place in objects)
    {
        for(NSString * property in allProperties)
        {
            
            [fujPlace setValue:[place valueForKey:property] forKey:property];
            
        }
        [places addObject:fujPlace];
    }

    
    return [NSArray arrayWithArray:places];
}

-(FujPlace*)getPlaceFromFujPlaceID:(NSString*)placeId
{
    NSFetchRequest *fetchRequest = [NSFetchRequest fetchRequestWithEntityName:@"Place"];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Place" inManagedObjectContext:self.managedObjectContext];
    
    NSPredicate *predicate       = [NSPredicate predicateWithFormat:@"p_id==%@",placeId];
    
    [fetchRequest setEntity:entity];
    fetchRequest.predicate = predicate;
    
    NSArray *objects = [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Place.class];

    FujPlace * fujPlace = [[FujPlace alloc] init];
    Place * place = [objects lastObject];
    for(NSString * property in allProperties)
    {
        
        [fujPlace setValue:[place valueForKey:property] forKey:property];
        
    }
    
    return fujPlace;
}

-(NSMutableArray *)getPlacesBycategory:(FujPlacesCateogries*)cat
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Place"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    FujCatsAndPlaces * catAndPlaces = [[FujCatsAndPlaces alloc] init];
    NSArray * placesIds = [catAndPlaces getAllPlacesIdsFromCategory:cat];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"ANY Place.p_id IN %@",placesIds];
    [fetchRequest setPredicate:predicate];
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Place.class];
    NSMutableArray * entities = [[NSMutableArray alloc] init];
    
    for(Place * place in objects)
    {
        FujPlace * fujPlace = [[FujPlace alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [fujPlace setValue:[place valueForKey:property] forKey:property];
            
        }
        
        [entities addObject:fujPlace];
    }
    
    return entities;
}


-(void)deleteAllObjects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Place"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for(NSManagedObject * object in objects)
        [self.managedObjectContext deleteObject:object];
    
}



@end
