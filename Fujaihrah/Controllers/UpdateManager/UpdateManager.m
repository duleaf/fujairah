//
//  UpdateManager.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "UpdateManager.h"
@interface UpdateManager ()
{
   NSArray * currentInfo ;
    NSMutableArray * notificationInfo;
}
@end

@implementation UpdateManager


-(void)updateAllSections
{
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ListOfWebservices" ofType:@"plist"];
    
     notificationInfo = [NSMutableArray arrayWithContentsOfFile:path];
    
    NSArray * info = [notificationInfo lastObject];
    
    [self updateNewSection:info];
}

-(void)updateNewSection:(NSArray*)info
{
    currentInfo = info;
    NSString * classTag = [currentInfo objectAtIndex:1];
    UpdateController * updater = [[NSClassFromString([NSString stringWithFormat:@"%@UpdateController",classTag]) alloc] init];
    
    updater.delegate = self;
    updater.requestUri = [currentInfo firstObject];
    [updater StartUpdating];
    
}

-(void)UpdateFinished
{
    if(notificationInfo.count < 1)
        return;
    
    if(notificationInfo.count >= 1)
        [notificationInfo removeLastObject];
    
    
    
     [[NSNotificationCenter defaultCenter] postNotificationName:[currentInfo objectAtIndex:2] object:nil userInfo:nil];
    
    [self updateNewSection:[notificationInfo lastObject]];
    
    
    
}

@end
