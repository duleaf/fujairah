//
//  FujHistory.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١١‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujHistory.h"
#import "History.h"
#import "Utitlities.h"

@implementation FujHistory


-(int)numberOfAvailableRecords
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"History" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSLog(@"Count: %i",(int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil]);
    
    return (int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
}


-(void)insertObjects:(NSArray *)objects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"History" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:History.class];
    
    
    for(FujHistory * fujhistory in objects)
    {
        History * history = [NSEntityDescription insertNewObjectForEntityForName:@"History" inManagedObjectContext:self.managedObjectContext];
        
        for(NSString * property in allProperties)
        {
            
            
            [history setValue:[fujhistory valueForKey:property] forKey:property];
            
        }
        
        [self.managedObjectContext save:nil];
    }
    
}

-(NSArray *)getHistory
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"History"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:History.class];
    NSMutableArray * articals = [[NSMutableArray alloc] init];
    
    for(History * history in objects)
    {
        FujHistory * hist = [[FujHistory alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [hist setValue:[history valueForKey:property] forKey:property];
            
        }
        
        [articals addObject:hist];
    }
    
    return [NSArray arrayWithArray:articals];
}

-(void)deleteAllObjects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"History"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for(NSManagedObject * object in objects)
        [self.managedObjectContext deleteObject:object];
    
}

@end
