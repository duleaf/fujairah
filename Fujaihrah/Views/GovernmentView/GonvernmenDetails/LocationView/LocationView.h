//
//  LocationView.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>


@interface LocationView : UIView<CLLocationManagerDelegate,MKMapViewDelegate>

@property (nonatomic,strong) IBOutlet MKMapView *iMapView;
@property (nonatomic,strong) NSMutableArray * locations;

-(void)drawLocations;

-(void) drawRootForm:(CLLocationCoordinate2D)source ToDestination:(CLLocationCoordinate2D)dest;
-(void) drawRootFormMyLocationToDestination:(CLLocationCoordinate2D)dest;
-(CLLocationCoordinate2D) getLocation;

@end

