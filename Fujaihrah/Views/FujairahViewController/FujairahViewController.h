//
//  FujairahViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٨‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FujairahViewController : UIViewController


-(void)reloadCurrenData:(NSNotification*)userInfo;
-(void) showLoading;
-(void) hideLoading;



@end
