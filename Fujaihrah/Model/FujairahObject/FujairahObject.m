//
//  FujairahObject.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujairahObject.h"
#import "AppDelegate.h"

@implementation FujairahObject

-(id)init
{
    self= [super init];
    
    if(self)
    {
        AppDelegate* appDelegate = (AppDelegate*) [UIApplication sharedApplication].delegate;
        //2
        
        NSPersistentStoreCoordinator *coordinator = [appDelegate persistentStoreCoordinator];
        if (coordinator != nil) {
            self.managedObjectContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
            [self.managedObjectContext setPersistentStoreCoordinator: coordinator];
        }
        
    }
    
    return self;
    
}

@end
