//
//  NewsTableViewCell.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView * img;


@end
