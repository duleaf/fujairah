//
//  PlacesCateogries.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface PlacesCateogries : NSManagedObject

@property (nonatomic, retain) NSString * cat_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * name_ar;

@end
