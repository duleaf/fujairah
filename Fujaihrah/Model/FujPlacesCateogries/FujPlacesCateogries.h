//
//  FujPlacesCateogries.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FujairahObject.h"
@interface FujPlacesCateogries : FujairahObject

@property (nonatomic, retain) NSString * cat_id;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * name_ar;

-(NSArray *)getAllCategories;

@end
