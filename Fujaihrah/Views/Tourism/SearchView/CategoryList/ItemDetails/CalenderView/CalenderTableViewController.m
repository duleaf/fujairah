//
//  CalenderTableViewController.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "CalenderTableViewController.h"

@interface CalenderTableViewController ()
{
bool isStartOpen;
bool isEndOpen;
}

@end

@implementation CalenderTableViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    _plan = [[FujPlan alloc] init];
    _plan.place = _place;
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

-(void)viewWillAppear:(BOOL)animated
{
    isEndOpen   = false;
    isStartOpen = false;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:[NSString stringWithFormat:@"cell%i",(int)indexPath.section]];
    
    if (cell == nil) {
        NSString * nibFileName =@"CalenderTableViewControllerCells";
        
//        if(!([[UserPreferences getCurrnetlanguage] isEqualToString:ENGLISH_LANG]))
//            nibFileName = [NSString stringWithFormat:@"CategoryListTableViewCell_%@",[UserPreferences getCurrnetlanguage]];
        
        cell = [[[NSBundle mainBundle] loadNibNamed:nibFileName owner:nil options:nil] objectAtIndex:indexPath.section];
        
        if (indexPath.section == 1)
        {
            _startPicker = (UIDatePicker*)[cell viewWithTag:3];
//            _startTime   = (UILabel*)[cell viewWithTag:2];
        }
        
        else if (indexPath.section == 0)
        {
            _eventTitle = (UITextField*)[cell viewWithTag:2];
            _eventTitle.text = _place.name;
        }
    }
    

    
//    else if (indexPath.section == 2)
//    {
//        _endPicker = (UIDatePicker*)[cell viewWithTag:3];
//    }
    

    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{


    
    if ((indexPath. section == 1 ))
        return 212;
    
    else
        return 44;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
//    if (indexPath.section != 0)
//    {
//        if (indexPath.section == 1)
//        {
//            isEndOpen = false;
//            if (isStartOpen)
//            {
//                isStartOpen = false;
//            }
//            
//            else
//                isStartOpen = true;
//        }
//        
//        if (indexPath.section == 2)
//        {
//            isStartOpen = false;
//            if (isEndOpen)
//            {
//                isEndOpen = false;
//            }
//            
//            else
//                isEndOpen = true;
//        }
//
//        
//        [self.tableView reloadData];
//    }
}
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Table view delegate

// In a xib-based application, navigation from a table can be handled in -tableView:didSelectRowAtIndexPath:
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Navigation logic may go here, for example:
    // Create the next view controller.
    <#DetailViewController#> *detailViewController = [[<#DetailViewController#> alloc] initWithNibName:<#@"Nib name"#> bundle:nil];
    
    // Pass the selected object to the new view controller.
    
    // Push the view controller.
    [self.navigationController pushViewController:detailViewController animated:YES];
}
*/

@end
