//
//  Artical.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٩‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujArtical.h"
#import "Artical.h"
#import "Utitlities.h"

@implementation FujArtical



-(int)numberOfAvailableRecords
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Artical" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSLog(@"Count: %i",(int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil]);

    return (int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
}


-(void)insertObjects:(NSArray *)objects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Artical" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Artical.class];
    
    
    for(FujArtical * post in objects)
    {
        Artical * artical = [NSEntityDescription insertNewObjectForEntityForName:@"Artical" inManagedObjectContext:self.managedObjectContext];
        
        for(NSString * property in allProperties)
            {
                
                
                [artical setValue:[post valueForKey:property] forKey:property];
                
            }
        
        [self.managedObjectContext save:nil];
    }
    
}

-(NSArray *)getAllArticals
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Artical"];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"articalType == %@",NORMAL_NEWS];
    [fetchRequest setPredicate:predicate];
    
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Artical.class];
    NSMutableArray * articals = [[NSMutableArray alloc] init];
    
    for(Artical * artical in objects)
    {
        FujArtical * post = [[FujArtical alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [post setValue:[artical valueForKey:property] forKey:property];
            
        }
        
        [articals addObject:post];
    }
    
    return [NSArray arrayWithArray:articals];
}

-(NSArray *)getAllSocialArticals
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Artical"];
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"articalType != %@",NORMAL_NEWS];
    [fetchRequest setPredicate:predicate];
    
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Artical.class];
    NSMutableArray * articals = [[NSMutableArray alloc] init];
    
    for(Artical * artical in objects)
    {
        FujArtical * post = [[FujArtical alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [post setValue:[artical valueForKey:property] forKey:property];
            
        }
        
        [articals addObject:post];
    }
    
    return [NSArray arrayWithArray:articals];
}


-(void)deleteAllObjects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Artical"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for(NSManagedObject * object in objects)
        [self.managedObjectContext deleteObject:object];
    
}

@end
