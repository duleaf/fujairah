//
//  SplashViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "SplashViewController.h"
#import "AppDelegate.h"
@interface SplashViewController ()

@end

@implementation SplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    

    self.image.hidden = YES;
    self.ar.hidden = YES;
    self.en.hidden = YES;
    
    if([UIScreen mainScreen].bounds.size.height > 500)
    {
        [self.splashImage setImage:[UIImage imageNamed:@"fujairah_splash-1.png"]];
    }
    else
    {
        [self.splashImage setImage:[UIImage imageNamed:@"fujairah_splash960.png"]];

    }
    

}

-(void)viewDidAppear:(BOOL)animated
{
        [NSTimer scheduledTimerWithTimeInterval:2.0f target:self selector:@selector(showImage) userInfo:nil repeats:NO];
}

-(void)showImage
{
    [UIView transitionWithView:self.image
                      duration:0.4
                       options:UIViewAnimationOptionTransitionCrossDissolve
                    animations:NULL
                    completion:^(BOOL finished){
                        
                        self.ar.hidden = NO;
                        self.en.hidden = NO;
                    }];
    
    self.image.hidden = NO;
}

-(IBAction)langSelected:(id)sender
{
    
    UIButton * button = (UIButton*) sender;
    NSString * controller ;
    
    
    UIStoryboard *storyBoard = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:nil];
	self.viewController = [[JASidePanelController alloc] init];
    self.viewController.shouldDelegateAutorotateToVisiblePanel = NO;
    
    
    
    if(button.tag ==1)
    {
        [LanguageController setCurrentLanguage:@"ar"];
        controller = @"rightnav";
        self.viewController.rightPanel = [storyBoard instantiateViewControllerWithIdentifier:controller];
        self.viewController.centerPanel = [storyBoard instantiateViewControllerWithIdentifier:@"homenav"];
        self.viewController.leftPanel = nil;
    }
    else
    {
        [LanguageController setCurrentLanguage:@"en"];
        controller = @"leftnav";
        self.viewController.rightPanel = nil;
        self.viewController.centerPanel = [storyBoard instantiateViewControllerWithIdentifier:@"homenav"];
        self.viewController.leftPanel = [storyBoard instantiateViewControllerWithIdentifier:controller];
    }
    
    
	
    AppDelegate * app = (AppDelegate*) [UIApplication sharedApplication].delegate;
    
	app.window.rootViewController = self.viewController;
    [app.window makeKeyAndVisible];
    
    [ ((AppDelegate*)[UIApplication sharedApplication].delegate).updateManager updateAllSections];

    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
