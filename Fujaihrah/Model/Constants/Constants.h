//
//  Constants.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٤‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#ifndef FUJ1_Constants_h
#define FUJ1_Constants_h

#define ARABIC_LANG       @"ar"
#define ENGLISH_LANG     @"en"
#define LANGUAGE_CHANGED @"change"

#define RED_COLOR   [UIColor colorWithRed:255.0/255.0f green:0.0f blue:0.0f alpha:1.0f]

#define BASE_URL @"http://hoangnhien.net/demo/fujairah/api/"

#define FACEBOOK_NEWS @"fb"
#define NORMAL_NEWS @"normal"
#define TWITTER_NEWS @"tw"

#endif
