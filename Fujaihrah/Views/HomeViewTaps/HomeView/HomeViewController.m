//
//  HomeViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "HomeViewController.h"
#import "HomeCell.h"
@interface HomeViewController ()
{
    NSArray*details;
}
@end

@implementation HomeViewController
@synthesize infoArray;

//home , government ,history , about

int cellIndex;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSArray*temArray1=[[NSArray alloc] initWithObjects:@"fuj_ruler.png",@"bg_heading_details.png", nil];
    NSArray*temArray2=[[NSArray alloc] initWithObjects:@"pic_2.png",@"bg_heading_details_left.png", nil];
    NSArray*temArray3=[[NSArray alloc] initWithObjects:@"pic_3.png",@"bg_heading_details.png", nil];
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
    {
       details=[[NSArray alloc] initWithObjects:@"حاكم الفجيرة",@"صاحب السمو الشيخ حمد بن محمد الشرقي" ,@"عن الفجيرة",@"المزيد من التفاصيل عن إمارة الفجيرة",@"تاريخ الفجيرة",@"نبذة عن تاريخ الفجيرة", nil];
        self.title = @"مرحبا في تطبيق الفجيرة";
    }
    
    else
    {
       details=[[NSArray alloc] initWithObjects:@"Ruler of Fujairah",@"His Highness Sheikh Hamad bin Mohammed Al Sharqi" ,@"About Fujairah",@"More details about the Emirate of Fujairah",@"History of Fujairah",@"About the history of Fujairah", nil];
        self.title = @"Welcome to mFujairah";
    }
    
    
    
    
    infoArray=[[NSMutableArray alloc] initWithObjects:temArray1,temArray2,temArray3,temArray2,temArray1, nil];
    currentCells = [[NSMutableArray alloc] init];
    cellIndex = 30;
    self.navigationController.navigationBarHidden = NO;
    UIViewController * viewc = [((UINavigationController*)self.sidePanelController.rightPanel).viewControllers objectAtIndex:0];
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"en"])
    {
        self.navigationItem.rightBarButtonItem = nil;
    }
    else
    {
        UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"menu button.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(menuButtonPressed:)forControlEvents:UIControlEventTouchUpInside];
        [button setFrame:CGRectMake(0, 5, 27, 30)];
        
        _barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        self.navigationItem.rightBarButtonItem = _barButton;
        [_barButton setTarget:viewc];
        [_barButton setAction:@selector(menuButtonPressed:)];
    }
    
    
	// Do any additional setup after loading the view.
}



#pragma mark UITableViewDelegate / UITableViewDatasource Methods
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    return 152.0;
    
}
-(void)viewDidAppear:(BOOL)animated
{
     HomeCell* firstCell = (HomeCell*) [currentCells objectAtIndex:0];
    
    if(! firstCell.isAnimated)
    for (int i = 0; i < currentCells.count; i++)
    {
       HomeCell* cell = (HomeCell*) [currentCells objectAtIndex:i];
        UIView *imageView = [cell viewWithTag:10];
        
        UIView *labelView = [cell viewWithTag:20];
        

        if(i %2 == 0)
        {
            cell.isAnimated = YES;
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 imageView.frame = CGRectMake( imageView.frame.origin.x - 165, imageView.frame.origin.y, imageView.frame.size.width  , imageView.frame.size.height);
                                 
//                                 labelView.frame = CGRectMake( labelView.frame.origin.x + 155, labelView.frame.origin.y, labelView.frame.size.width , labelView.frame.size.height);
                             }
                             completion:nil];
        }
        else{
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 imageView.frame = CGRectMake( imageView.frame.origin.x + 165, imageView.frame.origin.y, imageView.frame.size.width , imageView.frame.size.height);
                                 
                                 labelView.frame = CGRectMake( 0, labelView.frame.origin.y, labelView.frame.size.width , labelView.frame.size.height);
                                 
                             }
                             completion:nil];
    
        }
    }
    cellIndex =  (int)currentCells.count -1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    // return [[[self fetchedResultsController]fetchedObjects]count];
    
    
    //Acordint to section approches
    //
    //	NSArray *sections = [[self fetchedResultsController] sections];
    //	if (tableView == self.searchDisplayController.searchResultsTableView)
    //	{
    //        return [self.searchResults count];
    //    }else if (sectionIndex < [sections count])
    //	{
    //		id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:sectionIndex];
    //		return sectionInfo.numberOfObjects;
    //	}
	
	return 3;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    HomeCell *cell = nil;

    if(indexPath.row < currentCells.count)
    cell = [currentCells objectAtIndex:indexPath.row];
    
    UIView *imageView = [cell viewWithTag:10];
    
    UIView *labelView = [cell viewWithTag:20];
    
    if (cell  == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:nil options:nil] firstObject] ;
        
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
        imageView = [[[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:nil options:nil] objectAtIndex:1];
        
        labelView = [[[NSBundle mainBundle] loadNibNamed:@"HomeCell" owner:nil options:nil] objectAtIndex:2];
        cell.isAnimated = false;
        
        [cell addSubview:labelView];
        [cell addSubview:imageView];
        
        UILabel * title = (UILabel *) [labelView viewWithTag:20];
        UILabel * detailsLable = (UILabel *) [labelView viewWithTag:35];
        
        title.text = [details objectAtIndex:indexPath.row * 2];
        detailsLable.text = [details objectAtIndex:(indexPath.row * 2) + 1];
        
        UIImageView * background = (UIImageView *) [labelView viewWithTag:3];

        NSLog(@"Position1 : %f",background.frame.origin.x);
        if(indexPath.row %2 ==1)
        {
            detailsLable .frame = CGRectMake(27, 68, 108, 62);
            background.frame = CGRectMake(0 , 0, 160, 150);
            background.backgroundColor = [UIColor clearColor];
        }
//        background.frame = CGRectMake(160, 0, 160, 150);

        NSLog(@"Position2 : %f",background.frame.origin.x);

        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIImageView * firstImg = (UIImageView *)[imageView viewWithTag:1];
        
        UIImageView * secImg = (UIImageView *)[imageView viewWithTag:2];
        
        UIImageView * thirdImg = (UIImageView *)[labelView viewWithTag:3];
        
        
        firstImg.image=[UIImage imageNamed:[[infoArray objectAtIndex:indexPath.row] objectAtIndex:0]];
        secImg.image=[UIImage imageNamed:[[infoArray objectAtIndex:indexPath.row] objectAtIndex:0]];
        if(indexPath.row %2 ==0)
        thirdImg.image = [UIImage imageNamed:[[infoArray objectAtIndex:indexPath.row] objectAtIndex:1]];
        

        [currentCells addObject:cell];
        
    }
    

    if((!cell.isAnimated) && cellIndex < indexPath.row)
    {

        if(indexPath.row %2 == 0)
        {
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 imageView.frame = CGRectMake( imageView.frame.origin.x - 165, imageView.frame.origin.y, imageView.frame.size.width , imageView.frame.size.height);
                                 
//                                 labelView.frame = CGRectMake( labelView.frame.origin.x + 155, labelView.frame.origin.y, labelView.frame.size.width , labelView.frame.size.height);
                             }
                             completion:nil];
        }
        else{
            [UIView animateWithDuration:0.5 delay:0.0 options:0
                             animations:^{
                                 imageView.frame = CGRectMake( imageView.frame.origin.x + 160, imageView.frame.origin.y, imageView.frame.size.width , imageView.frame.size.height);
                                 
                                 labelView.frame = CGRectMake( -165, labelView.frame.origin.y, labelView.frame.size.width , labelView.frame.size.height);
                                 
                             }
                             completion:nil];
            
        }
        cell.isAnimated = true;

    }
    
    return cell;
    
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UIViewController * viewC = nil;//
    
    
    if (indexPath.row == 0)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"fujruler"];
    }
    else if (indexPath.row == 1)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutuae"];
    }
    
    else if (indexPath.row == 2)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"fujhistory"];
    }
    
    [self.navigationController pushViewController:viewC animated:YES];
}


-(IBAction)menuButtonPressed:(id)sender
{
    [self.sidePanelController toggleRightPanel:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
