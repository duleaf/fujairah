//
//  ItemDetailsViewController.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "ItemDetailsViewController.h"
#import "CalenderView/CalenderTableViewController.h"
#import "Constants.h"
#import "Utitlities.h"
#import "LocationView.h"
@interface ItemDetailsViewController ()
{
    CalenderTableViewController *calender ;
}
@property (nonatomic, strong) EKEventStore *eventStore;
// Default calendar associated with the above event store
@property (nonatomic, strong) EKCalendar *defaultCalendar;

@property (nonatomic, strong) CalenderTableViewController *cander;

// Array of all events happening within the next 24 hours

@end

@implementation ItemDetailsViewController
int height,startPoint;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];

   
    // Check whether we are authorized to access Calendar
    [self checkEventStoreAccessForCalendar];

}


-(IBAction)shareButtonPressed:(id)sender
{
    int buttonId = ((UIButton*)sender).tag;
    
    if (buttonId == 1)
    {
        [self callPhone:nil];
    }
    else if (buttonId == 2)
    {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setSubject:@"My Subject"];
        [controller setMessageBody:@"Hello there." isHTML:NO];
        if (controller)
            [self presentViewController:controller animated:YES completion: nil];
    }
    
    else if (buttonId == 3)
    {
        UIViewController * mapView = [[UIViewController alloc] init];
        LocationView*  locationView = [[[NSBundle mainBundle] loadNibNamed:@"LocationView" owner:nil options:nil] firstObject];
        
        locationView.frame = mapView.view.frame;
        locationView.iMapView.frame = mapView.view.frame;
        
        [mapView.view addSubview:locationView];
        
        mapView.title = @"Title";
        
        [self.navigationController pushViewController:mapView animated:YES];
        
        mapView.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
    }
    else if (buttonId == 4)
    {
        [self openLink:@"fb://profile"];
    }
//    else if (buttonId == 5)
//    {
//        [self openLink:@"https://twitter.com/"];
//    }
    
    
}



-(IBAction)callPhone:(id)sender
{
    UIDevice *device = [UIDevice currentDevice];
    if ([[device model] isEqualToString:@"iPhone"] ) {
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",@"0097192227000"]]];
    } else {
        UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [notPermitted show];
    }
}

-(IBAction)openLink:(id)sender
{
    NSString * url ;
    UIButton * button = (UIButton*)sender;
    
    if (button.tag == 1)
    {
        url = self.place.fb_url;
        NSURL * nsurl = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",[url lastPathComponent]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:nsurl]){
            [[UIApplication sharedApplication] openURL:nsurl];
        }
        else {
            NSURL *link = [NSURL URLWithString:url];
            [[UIApplication sharedApplication] openURL:link];
        }
    }
    
    else if (button.tag == 2)
    {
        url = self.place.tw_url;
        
        if(self.place.tw_url.length < 1)
            return;
        
        NSURL * nsurl = [NSURL URLWithString:[NSString stringWithFormat:@"twitter://user?screen_name=%@",[url lastPathComponent]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:nsurl]){
            [[UIApplication sharedApplication] openURL:nsurl];
        }
        else {
            NSURL *link = [NSURL URLWithString:url];
            [[UIApplication sharedApplication] openURL:link];
        }
    }
    else
    {
        url = self.place.web;
        
        NSURL *link = [NSURL URLWithString:url];
        [[UIApplication sharedApplication] openURL:link];
        
    }
}

-(IBAction)showMap:(id)sender
{
    UIViewController * mapView = [[UIViewController alloc] init];
    
    LocationView*  locationView = [[[NSBundle mainBundle] loadNibNamed:@"LocationView" owner:nil options:nil] firstObject];
    
    locationView.frame = mapView.view.frame;
    locationView.iMapView.frame = mapView.view.frame;
    
    [mapView.view addSubview:locationView];
    
    mapView.title = self.place.name;
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        mapView.title = self.place.name_ar ;

    locationView.locations = [NSMutableArray arrayWithObject:self.place];
    [locationView drawLocations];
    [self.navigationController pushViewController:mapView animated:YES];
    
    mapView.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}





- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}


-(void) addPlan
{
    //    EKEventEditViewController *addController = [[EKEventEditViewController alloc] init];
    //
    //	// Set addController's event store to the current event store
    //	addController.eventStore = self.eventStore;
    //    addController.editViewDelegate = self;
    //    [self presentViewController:addController animated:YES completion:nil];
    
    UIStoryboard  *story = [UIStoryboard storyboardWithName:@"Main_iPhone" bundle:[NSBundle mainBundle]];
    //= [story instantiateViewControllerWithIdentifier:@"nav"];
    
    
    UINavigationController * navigator = [story instantiateViewControllerWithIdentifier:@"nav"];
    
    NSString * bTitle = @"Done";
    
    if ([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
    {
        bTitle = @"تم";
    }
    
    UIBarButtonItem *item ;
    
    calender = [navigator.viewControllers firstObject];
    calender.place = _place;
    
    item = (UIBarButtonItem*) calender.navigationItem.rightBarButtonItem;
    item.target = self ;
    [item setAction:@selector(addNewEvent)];
    [item setTitle:bTitle];
    
    [self presentViewController:navigator animated:YES completion:nil];
    
}



-(void)addNewEvent
{
    self.addToPlan = (UIButton*)[self.view viewWithTag:58];
    
    calender.plan.date = calender.startPicker.date;
    [calender.plan insertMeUp];
    if([[LanguageController getCurrentLanguage ] isEqualToString:@"en"])
        [self.addToPlan setTitle:@"Added to Plan - Edit" forState:UIControlStateNormal];
    else
        [self.addToPlan setTitle:@"اضيفت الي الخطة - تعديل" forState:UIControlStateNormal];
    
    
    EKEventStore *store = [[EKEventStore alloc] init];
    [store requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error) {
        if (!granted) { return; }
        EKEvent *event = [EKEvent eventWithEventStore:store];
        event.title = _place.name;
        if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
            event.title = _place.name_ar ;

        event.startDate = [NSDate date]; //today
        event.endDate = calender.startPicker.date;  //set 1 hour meeting
        [event setCalendar:[store defaultCalendarForNewEvents]];
        NSError *err = nil;
        [store saveEvent:event span:EKSpanThisEvent commit:YES error:&err];
    }];
    
 
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    startPoint = 287;
    
    self.articalDetails.frame = CGRectMake(20, startPoint + 10, 280, 20);
    
    
    self.eventStore = [[EKEventStore alloc] init];
    [_addToPlan addTarget:self action:@selector(addPlan) forControlEvents:UIControlEventTouchUpInside];
    self.title = _place.name;

    if([[LanguageController getCurrentLanguage ] isEqualToString:@"ar"])
    self.title = self.place.name_ar;
    
    FujPlan *plan = [[FujPlan alloc] init];
    plan.place = _place;
    if([plan isPlanExistWithPlace:_place])
    {
        if([[LanguageController getCurrentLanguage ] isEqualToString:@"en"])
            [self.addToPlan setTitle:@"Added to Plan - Edit" forState:UIControlStateNormal];
        else
            [self.addToPlan setTitle:@"اضيفت الي الخطة - تعديل" forState:UIControlStateNormal];
        
    }
    
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated
{
    [self loadWebView];
    [self.img  sd_setImageWithURL:[NSURL URLWithString:_place.cover_img_url]];
    
}
- (void)webViewDidFinishLoad:(UIWebView *)aWebView {

    height = [[aWebView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue];
    
    {
        self.articalDetails.frame = CGRectMake(self.articalDetails.frame.origin.x, self.articalDetails.frame.origin.y, self.articalDetails.frame.size.width, height);
        
        
        self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width, startPoint + height + 155);
       
    }
}

-(void)loadWebView
{
    
    //@"<p>1. You agree that you will be the technician servicing this work order?.<br>2. You are comfortable with the scope of work on this work order?.<br>3. You understand that if you go to site and fail to do quality repair for  any reason, you will not be paid?.<br>4. You must dress business casual when going on the work order.</p>"
    NSString * html = [Utitlities getHtmlFromatFor:_place.desc];
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        html = [Utitlities getHtmlFromatFor:_place.desc_ar] ;

    
    [self.articalDetails loadHTMLString:html baseURL:nil];
    self.articalDetails.backgroundColor = [UIColor clearColor];
    [self.articalDetails setOpaque:NO];
    NSLog(@"%f",self.articalDetails.scrollView.contentSize.height);
    self.articalDetails.scrollView.scrollEnabled = NO;
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark Access Calendar

// Check the authorization status of our application for Calendar
-(void)checkEventStoreAccessForCalendar
{
    EKAuthorizationStatus status = [EKEventStore authorizationStatusForEntityType:EKEntityTypeEvent];
    
    switch (status)
    {
            // Update our UI if the user has granted access to their Calendar
        case EKAuthorizationStatusAuthorized: [self accessGrantedForCalendar];
            break;
            // Prompt the user for access to Calendar if there is no definitive answer
        case EKAuthorizationStatusNotDetermined: [self requestCalendarAccess];
            break;
            // Display a message if the user has denied or restricted access to Calendar
        case EKAuthorizationStatusDenied:
        case EKAuthorizationStatusRestricted:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Privacy Warning" message:@"Permission was not granted for Calendar"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
            break;
        default:
            break;
    }
}
-(void)requestCalendarAccess
{
    [self.eventStore requestAccessToEntityType:EKEntityTypeEvent completion:^(BOOL granted, NSError *error)
     {
         if (granted)
         {
             ItemDetailsViewController * __weak weakSelf = self;
             // Let's ensure that our code will be executed from the main queue
             dispatch_async(dispatch_get_main_queue(), ^{
                 // The user has granted access to their Calendar; let's populate our UI with all events occuring in the next 24 hours.
                 [weakSelf accessGrantedForCalendar];
             });
         }
     }];
}


// This method is called when the user has granted permission to Calendar
-(void)accessGrantedForCalendar
{
    NSArray *eventCalendars = [_eventStore calendarsForEntityType:EKEntityTypeEvent];
    
    for (EKCalendar *eventCalendar in eventCalendars) {
        NSLog(@"an event calendar ... %@", eventCalendar.description);
    }
    
    EKSource *myLocalSource = nil;
    for (EKSource *calendarSource in _eventStore.sources) {
        if (calendarSource.sourceType == EKSourceTypeExchange) {
            myLocalSource = calendarSource;
            break;
        }
    }
    EKCalendar *myCalendar = [EKCalendar calendarForEntityType:EKEntityTypeEvent eventStore:_eventStore];
    myCalendar.title = @"Fujirah Calender";
    myCalendar.source = myLocalSource;
    
    NSError *error = nil;
    [_eventStore saveCalendar:myCalendar commit:YES error:&error];
    
    if(! error)
    {
        self.defaultCalendar = myCalendar;
    }
    else
    // Let's get the default calendar associated with our event store
    self.defaultCalendar = self.eventStore.defaultCalendarForNewEvents;
    // Enable the Add button
    // Fetch all events happening in the next 24 hours and put them into eventsList
    // Update the UI with the above events
    
    

}


// Overriding EKEventEditViewDelegate method to update event store according to user actions.
- (void)eventEditViewController:(EKEventEditViewController *)controller
		  didCompleteWithAction:(EKEventEditViewAction)action
{
//    ItemDetailsViewController * __weak weakSelf = self;
	// Dismiss the modal view controller
    [self dismissViewControllerAnimated:YES completion:^
     {

     }];
}


// Set the calendar edited by EKEventEditViewController to our chosen calendar - the default calendar.
- (EKCalendar *)eventEditViewControllerDefaultCalendarForNewEvents:(EKEventEditViewController *)controller
{
	return self.defaultCalendar;
}
@end
