//
//  GovEntityDetails.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٣‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FujairahObject.h"
@interface FujGovEntityDetails : FujairahObject


@property (nonatomic, retain) NSString * ent_id;
@property (nonatomic, retain) NSString * fb;
@property (nonatomic, retain) NSString * image;
@property (nonatomic, retain) NSString * intro;
@property (nonatomic, retain) NSString * lang;
@property (nonatomic, retain) NSString * lat;
@property (nonatomic, retain) NSString * logo;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * targets;
@property (nonatomic, retain) NSString * tw;
@property (nonatomic, retain) NSString * webSite;
@property (nonatomic, retain) NSString * intro_ar;
@property (nonatomic, retain) NSString * name_ar;


-(NSMutableArray *)getAllEntities;

@end
