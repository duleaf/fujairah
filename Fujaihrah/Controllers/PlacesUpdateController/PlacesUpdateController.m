//
//  PlacesUpdateController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١١‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "PlacesUpdateController.h"
#import "FujPlace.h"
#import "FujCatsAndPlaces.h"
@implementation PlacesUpdateController



-(void)StartUpdating
{
    self.fujObject = [[FujPlace alloc] init];
    
    
    if([self.fujObject numberOfAvailableRecords] > MIN_NUMBER_OF_ITEMS)
    {
        if ([self isNewUpdateAvailable])
        {
            ;
        }
        [self.delegate UpdateFinished];
    }
    else
    {
        [self.fujObject deleteAllObjects];
        [[[FujCatsAndPlaces alloc] init] deleteAllObjects];
        [self fetchAllData];
    }
    
}


-(BOOL) isNewUpdateAvailable
{
    return true;
}

-(void)updateCurrentDataBase
{
    
}

-(void)fetchAllData
{
    NSString * extraUri = @"";
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        extraUri = @"/?lang=ar";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@%@",BASE_URL,self.requestUri,extraUri] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray * places = [self ParssedObjects:responseObject];
        [self.fujObject insertObjects:places];
        [self.delegate UpdateFinished];
        //        id obj = [[NSClassFromString(@"class") alloc] init];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


-(NSArray*)ParssedObjects:(id)response
{
    NSDictionary * responseDic = (NSDictionary*) response;
    NSMutableArray * places = [[NSMutableArray alloc] init];
    NSArray * responsePosts = [responseDic valueForKey:@"places"];
    
    for(NSDictionary * post in responsePosts)
    {
        FujPlace * place = [[FujPlace alloc] init];
        
        place.name = [post valueForKey:@"title"];
        place.name_ar = [post valueForKey:@"title_ar"];
        NSNumber * number = [post valueForKey:@"id"];
        place.p_id = [NSString stringWithFormat:@"%i",number.intValue];
        place.desc = [post valueForKey:@"description"];
        place.desc_ar = [post valueForKey:@"description_ar"];
        place.lat = [post valueForKey:@"lat"];
        place.lang = [post valueForKey:@"long"];
        place.icon_url = [post valueForKey:@"logo"];
        place.cover_img_url = [post valueForKey:@"cover_image"];
        place.fb_url = [post valueForKey:@"facebook"];
        place.tw_url = [post valueForKey:@"twitter"];
        place.web = [post valueForKey:@"website"];
        place.phone = [post valueForKey:@"phone"];
        
        FujCatsAndPlaces *fujCatsAndPlaces;
        
        NSArray * cats = [responseDic valueForKey:@"categories"];
        NSMutableArray * catsAndPlaces = [[NSMutableArray alloc] init];
        for(NSDictionary * cat in cats)
        {
            fujCatsAndPlaces = [[FujCatsAndPlaces alloc] init];
            fujCatsAndPlaces.cat_id = [cat valueForKey:@"id"];
            fujCatsAndPlaces.p_id = place.p_id;
            [catsAndPlaces addObject:fujCatsAndPlaces];
        }
        [fujCatsAndPlaces insertObjects:[NSArray arrayWithArray:catsAndPlaces]];
        
        [places addObject:place];
        
    }
    
    return places;
}


@end
