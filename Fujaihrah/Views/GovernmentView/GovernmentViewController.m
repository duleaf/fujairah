//
//  GovernmentViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٥‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "GovernmentViewController.h"
#import "FujGovEntityDetails.h"
#import "GovernmentViewCell.h"
#import "GonvernmenDetailsViewController.h"
@interface GovernmentViewController ()
{
    UIActivityIndicatorView * loading;
}
@end

@implementation GovernmentViewController



- (void)viewDidLoad
{
    [super viewDidLoad];
    [self getEntities];
    
    
    if([[LanguageController  getCurrentLanguage] isEqualToString:@"ar"])
    {
        self.title = @"حكومة الفجيرة";
    }
    else
    {
        self.title = @"Fujairah Government";
    }
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(reloadCurrenData:)
     name:NSStringFromClass(self.class)
     object:nil];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 3;
}

- (NSInteger)numberOfSectionsInCollectionView: (UICollectionView *)collectionView {
    
    int reminder = (self.entities.count % 3)? 1:0;
    
    return (self.entities.count / 3 )+ reminder;
}



- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *identifier = @"cell";
    
    GovernmentViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
 
    int currentIndex = (3 * (int)indexPath.section) +(int) indexPath.row;
    
    if(currentIndex >= self.entities.count)
        return  nil;
    
    FujGovEntityDetails * entity = [self.entities objectAtIndex:currentIndex];
    
    [cell.image sd_setImageWithURL:[NSURL URLWithString:entity.logo] placeholderImage:nil];
    cell.image.backgroundColor = [UIColor whiteColor];

    [cell.image.layer setCornerRadius:4];
    cell.image.clipsToBounds = YES;
        


    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        cell.title.font = [UIFont systemFontOfSize:15];
    else
        cell.title.font = [UIFont systemFontOfSize:11];
    
    cell.title.textAlignment = NSTextAlignmentCenter;
    
    cell.title.text = entity.name;
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        cell.title.text = entity
        .name_ar ;

//        [cell.title sizeToFit];
  
    return cell;
}





-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    GonvernmenDetailsViewController * gov = [self.storyboard instantiateViewControllerWithIdentifier:@"govDetials"];
    int currentIndex = (3 * (int)indexPath.section) + (int)indexPath.row;
    gov.entity = [self.entities objectAtIndex:currentIndex];
    [self.navigationController pushViewController:gov animated:YES];
    

}

-(void) getEntities
{
    FujGovEntityDetails * entity = [[FujGovEntityDetails alloc] init] ;
    self.entities = [entity getAllEntities];
    
    if(self.entities.count == 0)
    {
        [self showLoading];
    }
//    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
//    {
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"هيئة المنطقة الحرة بالفجيرة ";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-fujairah.png";
//        entity.image = @"http://fujairah.ae/en/PublishingImages/Pages/freezone/Freezone.jpg";
//        entity.intro = @"تقع المنطقة الحرة بالفجيرة بجوارميناء الفجيرة، وتتمتع الشركات المؤسسة في المنطقة الحرة بسهولة الوصول إلى جميع موانئ الخليج العربي والبحر الأحمر وإيران والهند وباكستان على متن السفن التي ترفد إلى الميناء بشكل مستمر، وتصل خدمات الخطوط الرئيسية من أوروبا الشمالية والبحر الأبيض المتوسط ​​والشرق الأقصى وأمريكا الشمالية بشكل أسبوعي، وتغادر الخدمات مرتين في الأسبوع إلى الشرق الأقصى ومرة واحدة في الاسبوع لأمريكا الشمالية، كما تتميز المنطقة الحرة بالفجيرة بقربها من مطار الفجيرة الدولي وهو المطار الوحيد الذي يخدم الدولة على الساحل الشرقي. وبسبب الموقع الجغرافي وسهولة الوصول إلى طرق الشحن الرئيسية في العالم والقرب من ميناء ومطار دوليين بالإضافة إلى الإجراءات المبسطة أصبحت المنطقة الحرة بالفجيرة مكانا مثالياً لممارسة الأعمال التجارية. ";
//        
//        entity.targets = @"يستفيد المستثمرون من المزايا الثلاثية للمنطقة الحرة بالفجيرة وهي:        1)  سهولة الوصول: واللمسة الشخصية من الموظفين والإدارة.  2) الربط: تقدم المنطقة الحرة بالفجيرة وسائل ربط فريدة من نوعها - الربط اللوجستي بالعالم جواً عبر مطار الفجيرة الدولي، وبحراً من خلال ميناء الفجيرة، وبراً إلى منطقة الشرق الأوسط وخارجها. 3) التوفير الاقتصادي: يستفيد المستثمرين من الإجراءات الأسرع للمعاملات والمواعيد الأقصر في التسليم، كما تقدم المنطقة الحرة بالفجيرة عروضاً اقتصادية لا مثيل لها - من خلال رسوم أرخص والحد الأدنى لوقت البدء، حيث يمكن إصدار التراخيص في غضون يوم عمل واحد، هذا بالإضافة إلى نفقات الإنشاء المخفضة وانخفاض النفقات العامة مما جعل المنطقة الحرة بالفجيرة فعالة جداً من حيث التكلفة الاستثمارية.";
//        entity.phone = @"+971 9-222-8000";
//        entity.webSite = @"http://www.fujairahfreezone.com/uae.htm";
//        entity.fb = @"";
//        entity.tw = @"";
//        [self.entities addObject:entity];
//        
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"مطار الفجيرة";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-airport.png";
//        entity.image = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/fujairah_airport.jpg";
//        entity.intro = @"يتميز مطار الفجيرة الدولي بالابتكار في تقديم خدماته التكنولوجية وبخبرة ومهارة طاقم عمله مما جعل خدماته على مستوى عال من الجودة والتميز، وقد ابتدأ المطار الذي يعد خامس مطار في دولة الإمارات العربية المتحدة والمطار الوحيد للدولة على الساحل الشرقي بالعمل بتاريخ 29 أكتوبر 1987 في إمارة الفجيرة عاكساً بذلك أهمية الإمارة المتنامية كمركز لقطاع الطيران والتجارة والسياحة. ويضمن المطار لشركات الطيران شحن وتفريغ طائراتهم بشكل أسرع والراحة المثلى لركابهم بسبب تجهيزه بأفضل ما توصلت إليه تكنولوجيا الطيران الحديثة والشحن الجوي والمناولة ، ونظراً لموقعه الاستراتيجي فإن مطار الفجيرة الدولي يعتبر نقطة عبور مثالية ومركزاً للتداول في أعمال الشحن الجوي والبحري المزدهر بين الشرق والغرب، ويساعده على ذلك وجود ميناء الإمارة الحديث على مقربة منه، و ​بسبب سياسة الأجواء المفتوحة ومعدلات المناولة ذات القدرة التنافسية العالية وأسعار الوقود المنخفضة وعدم التعقيد في إجراءات الجمارك والهجرة بالإضافة إلى الخدمات الأكثر تخصيصاً وكفاءة فقد أضاف مطار الفجيرة الدولي أبعاداً جديدة إلى قطاع الطيران في دولة الإمارات العربية المتحدة.";
//        
//        entity.targets = @"";
//        
//        entity.phone = @"+971 9 222-6222";
//        entity.webSite = @"http://www.fujairah-airport.com";
//        entity.fb = @"";
//        entity.tw = @"";
//        [self.entities addObject:entity];
//        
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"هيئة الفجيرة للسياحة والآثار";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-tourism.png";
//        entity.image = @"http://fujairah.ae/ar/PublishingImages/Pages/FujairahTourismAntiquitiesAuthority/Fuj_Tourism.jpg";
//        entity.intro = @"شهدت إمارة الفجيرة تطوراً ملحوظاً في الآونة الأخيرة في جميع النواحي الاقتصادية والاجتماعية وخاصة في قطاع السياحة والضيافة وذلك بفضل القيادة الحكيمة لصاحب السمو الشيخ حمد بن محمد الشرقي عضو المجلس الأعلى حاكم الفجيرة وولي عهده سمو الشيخ محمد بن حمد الشرقي حيث بذلت كافة الجهود لتطوير الموارد التى تتمتع بها الإمارة والرقي بمستوى الخدمات والإنتاج في مختلف النواحي والمجالات التجارية والصناعية والسياحية والزراعية.  تأسست هيئة الفجيرة للسياحة والآثار بموجب مرسوم أميري رقم (3) لعام 2009 والذي أصدره صاحب السمو الشيخ حمد بن محمد الشرقي عضو المجلس الأعلى للاتحاد حاكم الفجيرة كهيئة مستقلة تتمتع بالشخصية الاعتبارية والقانونية .";
//        
//        entity.targets = @"";
//        
//        entity.phone = @"+971 9-223-1436";
//        entity.webSite = @"http://www.fujairah-airport.com";
//        entity.fb = @"https://www.facebook.com/fujairahtourism.ae";
//        entity.tw = @"https://twitter.com/fujtourism";
//        [self.entities addObject:entity];
//        
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"دائرة جمارك الفجيرة";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-custom.png";
//        entity.image = @"http://fujairah.ae/en/PublishingImages/Pages/customs/Customs.jpg";
//        entity.intro = @"تقدم دائرة جمارك الفجيرة مرافق جمركية فعالة ومشتركة لمستخدمي المنطقة الحرة بالفجيرة وميناء الفجيرة ومطار الفجيرة الدولي وميناء دبا الفجيرة، وقد تم تبسيط الإجراءات الرسمية لدائرة الجمارك على مدى فترة من الزمن مما أدى إلى عمليات مريحة وحركة أسرع في الشحن البحري/الجوي ";
//        
//        entity.targets = @"";
//        
//        entity.phone = @"+971 9 2282222";
//        entity.webSite = @"http://fujcustoms.gov.ae";
//        entity.fb = @"";
//        entity.tw = @"";
//        [self.entities addObject:entity];
//        
//        
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"بلدية الفجيرة";
//        entity.logo = @"http://www.fujmun.gov.ae//themes/images/logo.png";
//        entity.image = @"http://www.fujmun.gov.ae/uploads/slide8.jpg";
//        entity.intro = @"بلدية الفجيرة تأسست عام 1969 بموجب القانون المحلي رقم (1) لسنة 1969 البلدية هي مؤسسه حكومية محلية مختصة بالشؤن البدية الحضرية و القروية وتقديم الخدمات والعمال العامة بما يتوافق مع متطلبات التنمية، لها شخصة معنويه مستقلة، ولها هيكل تنظيمي و كوادر ادارية و فنية لممارسة الاختصاصات و المهام لتحقيق اهدافها.";
//        entity.targets = @"تشجيع النمو العمراني والتجاري والصناعي والاقتصادي والتنمية المستدامة ، وتشجيع إقامة المشاريع الكبرى وجذب الاستثمارات المحلية والإقليمية والأجنبية ، والاهتمام بالثروات الزراعية والحيوانية والسمكية بالتعاون مع الجهات المعنية ، وتسهيل إجراءات إصدار تصاريح المباني والرخص التجارية والصناعية والحرفية والمهنية وملكيات الأراضي والمباني وتصنيف المقاولين وعقود تأجير الأراضي الصناعية وعقود الرهن التجاري والمدني . تنظيم ومراقبة أسواق الجملة والمفرق لمنشآت تداول المواد الغذائية ، وأسواق تجارة الهدايا والملابس والأدوات والأجهزة الكهربائية والالكترونية والصحية والكهرومغناطيسية والهواتف بأنواعها والأجهزة الرياضية والمفروشات، والمحال الحرفية والمناطق الصناعية.     الاهتمام بالبنية التحتية واستخدامات الأراضي والتخطيط العمراني .  الحفاظ على النظافة والصحة العامة والحياة الفطرية ، وحماية البيئة من التلوث وتنميتها ، وإنشاء وإدارة المناطق المحمية البحرية والبرية .حماية المستهلك بالتعاون مع الجهات المعنية من الغش التجاري والتنزيلات الوهمية والإعلانات المضللة، والمساهمة في نشر الوعي الاستهلاكي حول السلع والخدمات .  تشغيل وإدارة وتطوير مختبرات البلدية في مجالات فحص المياه والغذاء واللحوم والهواء ومواد البناء وغيرها .";
//        entity.phone = @"+971 9-222-7000";
//        entity.webSite = @"http://www.fujmun.gov.ae/default.aspx";
//        entity.fb = @"https://www.facebook.com/FujairahMunicipality";
//        entity.tw = @"https://twitter.com/fujmun";
//        [self.entities addObject:entity];
//        
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"ميناء الفجيرة";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-port.png";
//        entity.image = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/fujairah_port.jpg";
//        entity.intro = @"ميناء الفجيرة هو الميناء الوحيد المتعدد الأغراض على الساحل الشرقي لدولة الإمارات العربية المتحدة، ويبعد ما يقرب من 70 ميلاً بحرياً فقط عن مضيق هرمز .         ابتدأت مرحلة إنشاء الميناء الأولية في عام 1978م  كجزء من التنمية الاقتصادية لدولة الإمارات العربية المتحدة، وابتدأ تشغيل عملياته بشكل كامل في عام 1983م، ومنذ ذلك الحين بدأ الميناء سلسلةً مستمرة من التحسينات لمجموعة الوظائف الشاملة والمرافق الموجودة به .";
//        entity.targets = @"";
//        entity.phone = @"+971 9-222-8800";
//        entity.webSite = @"http://www.fujairahport.ae";
//        entity.fb = @"";
//        entity.tw = @"";
//        [self.entities addObject:entity];
//        
//    }
//    else
//    {
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"Fujairah Free Zone​​";
//        entity.logo = @"http://www.fujmun.gov.ae//themes/images/logo.png";
//        entity.image = @"http://fujairah.ae/en/PublishingImages/Pages/freezone/Freezone.jpg";
//        entity.intro = @"Fujairah Free Zone is adjacent to the Port of Fujairah. Companies established here have easy access to all Arabian Gulf ports, the Red Sea, Iran, India and Pakistan on weekly feeder vessels. Mainline services arrive from Northern Europe, the Mediterranean, Far East and North America on a weekly basis, and services leave twice​​ weekly to the Far East and once a week to North America. Fujairah Free Zone is also close to Fujairah International Airport which is the only airport serving the UAE East Coast as well as northern Oman. A fortunate combination of geographic location, with access to world's major shipping routes, a fine port & airport, and streamlined procedures, make the Free Zone of Fujairah an ideal place for business.";
//        
//        entity.targets = @"Investors benefit from the triple Fujairah Free Zone advantage, namely the ACE -Accessibility, Connectivity & Economy. Accessibility & personal touch of the staff, and management. Fujairah Free Zone offers a unique Connectivity - logistic link to the world; by air through Fujairah International Airport, by sea through Fujairah Sea Port, and by road to Middle East & beyond. Investors benefit by way of faster transaction, accruing due to shorter delivery times. Fujairah Free Zone (FFZ) offers an unmatched Economy - cheaper tariffs, and minimum start up time. Licenses can be issued within one working day. Reduced establishment expenses, and lower overheads, make FFZ a very cost effective investment proposition.";
//        
//        entity.phone = @"+971 9-222-8000";
//        entity.webSite = @"http://www.fujairahfreezone.com/uae.htm";
//        entity.fb = @"";
//        entity.tw = @"";
//        [self.entities addObject:entity];
//        
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"Fujairah Airport";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-airport.png";
//        entity.image = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/fujairah_airport.jpg";
//        entity.intro = @"The dominant presence of cutting edge innovation in technology on one end and skillfully experienced manpower on the other makes Fujairah International Airport's Terminal Services world class From a flight of fancy to a living reality, UAE's fifth and only airport on the east coast became operational on October 29,1987 at Fujairah, reflecting the Emirate's growing importance as a center for aviation, commerce and tourism. Equipped with all that is best in modern aviation and air-cargo handling technology, the airport ensures quicker turnaround for airlines and optimum comfort for passengers. Strategically located, Fujairah International Airport is the ideal trading and transit point in the booming sea-air cargo business between East and West, with the Emirate's modern seaport in close proximity. With an Open-Skies Policy, highly competitive handling rates, low fuel prices, no-fuss customs and immigration, and more personalized and efficient service, Fujairah International Airport has added new dimensions to the UAE's aviation scene.";
//        
//        entity.targets = @"";
//        
//        entity.phone = @"+971 9 222-6222";
//        entity.webSite = @"http://www.fujairah-airport.com";
//        entity.fb = @"";
//        entity.tw = @"";
//        [self.entities addObject:entity];
//        
//        
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"Fujairah Tourism & Antiquities Authority";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-tourism.png";
//        entity.image = @"http://fujairah.ae/ar/PublishingImages/Pages/FujairahTourismAntiquitiesAuthority/Fuj_Tourism.jpg";
//        entity.intro = @"The Emirate of Fujairah has witnessed remarkable growth in recent times in all aspects social and economic activities. The tourism and hospitality sector also has grown by leaps and bounds - thanks to the wise leadership of His Highness Sheikh Hamad bin Mohammed Al Sharqi, Supreme Council Member & Ruler of Fujairah and His Highness Sheikh Mohammed bin Hamad Al Sharqi Crown Prince of Fujairah. The Ruler and Crown Prince of Fujairah have taken all steps to develop the infrastructure of the Emirate and utilize the available resources to achieve better levels of services and production in the trade and industry, tourism and agriculture sectors.";
//        
//        entity.targets = @"";
//        
//        entity.phone = @"+971 9-223-1436";
//        entity.webSite = @"http://www.fujairah-airport.com";
//        entity.fb = @"https://www.facebook.com/fujairahtourism.ae";
//        entity.tw = @"https://twitter.com/fujtourism";
//        [self.entities addObject:entity];
//        
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"Fujairah Customs Department​ ";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-custom.png";
//        entity.image = @"http://fujairah.ae/en/PublishingImages/Pages/customs/Customs.jpg";
//        entity.intro = @"The Department offers efficient and common customs facilities for users of the Free Zone of Fujairah, Port of Fujairah, Fujairah International Airport and the Dibba Sea Port. The customs procedures and formalities have been simplified, leading to convenient operations and faster movement of sea/air cargo. ";
//        
//        entity.targets = @"";
//        
//        entity.phone = @"+971 9 2282222";
//        entity.webSite = @"http://fujcustoms.gov.ae";
//        entity.fb = @"";
//        entity.tw = @"";
//        [self.entities addObject:entity];
//        
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"Fujairah Municipality";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-fujairah.png";
//        entity.image = @"http://www.fujmun.gov.ae/uploads/slide8.jpg";
//        entity.intro = @"Fujairah Municipality was founded in 1969 vide local act no (1) for year 1969. The municipality is a local government organization specialized in municipal urban and rural municipal affairs. It is responsible for the provision of public services consistent with the development requirements. The municipality is an independent entity with its own organizational structure and own administrative and technical cadres to carry out their assigned duties and tasks and to achieve their objectives. ";
//        
//        entity.targets = @" Enforcement on the growth of the urban, commercial, industrial, economic development and sustainable development as well as encouraging the launching of major projects and attracting local, regional and international investment. It also supports the development of the agricultural, animal and fishing resources, in cooperation with the concerned parties. While simplifying procedures for issuing permits such as commercial, industrial, trade licenses, land and building ownership, contractors classification, industrial land leasing, commercial and real state mortgage certifications.Organization and monitoring wholesale and retail markets dealing with food stuffs, gifts, garment, electrical and electronic, instruments and equipment, sanitary, electromagnetic, telephones (all types), sports equipment, furniture, trade shops and industrial areas. Attending to the infrastructure and land usage and urban planning.Maintaining hygiene, public health, wild life, the protection of the environment from contamination and the establishment of reservations for marine and wild life.Customer protection in cooperation with concerned the parties from commercial fraud, fraudulent sale campaigns and misguiding advertisements, and participation in creating consumer awareness on commodities and services.Operation, management and development of the municipality laboratories for testing water, foods, meat, air, building material and others.";
//        entity.phone = @"+971 9-222-7000";
//        entity.webSite = @"http://www.fujmun.gov.ae/default.aspx";
//        entity.fb = @"https://www.facebook.com/FujairahMunicipality";
//        entity.tw = @"https://twitter.com/fujmun";
//        [self.entities addObject:entity];
//        
//        entity = [[FujGovEntityDetails alloc] init];
//        entity.name = @"Fujairah Port​ ";
//        entity.logo = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/department-port.png";
//        entity.image = @"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/fujairah_port.jpg";
//        entity.intro = @"Port of Fujairah is the only multi-purpose port on the Eastern seaboard of the United Arab Emirates, approximately 70 nautical miles from the Straits of Hormuz. Initial Construction of the Port started in 1978 as part of the economic development of the UAE. Full operations commenced in 1983. Since then the Port has embarked on a continuing process of enhancement to both its facilities and its comprehensive range of functions.";
//        entity.targets = @"";
//        entity.phone = @"+971 9-222-8800";
//        entity.webSite = @"http://www.fujairahport.ae";
//        entity.fb = @"";
//        entity.tw = @"";
//        [self.entities addObject:entity];
//    }
//
    
   
}
    

-(void)reloadCurrenData:(NSNotification*)userInfo
{
    [self getEntities];
    [self hideLoading];
    [self.collectionView reloadData];
    
}



- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}


-(void) showLoading
{
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loading.center = self.view.center;
    
    [loading startAnimating];
    [self.view addSubview:loading];
}

-(void) hideLoading
{
    [loading startAnimating];
    [loading removeFromSuperview];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
