//
//  LanguageController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٥‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LanguageController : NSObject

+(void)setCurrentLanguage:(NSString*)lang;
+(NSString*) getCurrentLanguage;
+(NSString*) localizedStringForKey:(NSString*)key;

@end
