//
//  UpdateManager.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UpdateController.h"

@interface UpdateManager : NSObject <UpdateDelegate>

-(void)updateAllSections;

@end
