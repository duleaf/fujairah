//
//  Plan.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Plan : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSString * place_id;

@end
