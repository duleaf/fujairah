//
//  NewsHolderViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٩‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NewsDetialsViewController.h"
#import "NewsViewController.h"
@protocol ItemSelected <NSObject>

-(void)itemSelected:(NewsDetialsViewController*) detialsNews;

@end


@interface NewsHolderViewController : UITableViewController <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic,strong) NSString * type;
@property (nonatomic,strong) NSArray * articals;
@property (nonatomic,strong) NewsViewController * parentView ;

-(void) showLoading;
-(void) hideLoading;


@end
