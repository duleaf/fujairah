//
//  FujRurler.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١١‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujRurler.h"
#import "Ruler.h"
#import "Utitlities.h"
@implementation FujRurler

-(int)numberOfAvailableRecords
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Ruler" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSLog(@"Count: %i",(int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil]);
    
    return (int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
}


-(void)insertObjects:(NSArray *)objects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"Ruler" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Ruler.class];
    
    
    for(FujRurler * fujRuler in objects)
    {
        Ruler * ruler = [NSEntityDescription insertNewObjectForEntityForName:@"Ruler" inManagedObjectContext:self.managedObjectContext];
        
        for(NSString * property in allProperties)
        {
            
            
            [ruler setValue:[fujRuler valueForKey:property] forKey:property];
            
        }
        
        [self.managedObjectContext save:nil];
    }
    
}

-(NSArray *)geRuler
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Ruler"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:Ruler.class];
    NSMutableArray * articals = [[NSMutableArray alloc] init];
    
    for(Ruler * ruler in objects)
    {
        FujRurler * fujRuler = [[FujRurler alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [fujRuler setValue:[ruler valueForKey:property] forKey:property];
            
        }
        
        [articals addObject:fujRuler];
    }
    
    return [NSArray arrayWithArray:articals];
}

-(void)deleteAllObjects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Ruler"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for(NSManagedObject * object in objects)
        [self.managedObjectContext deleteObject:object];
    
}

@end
