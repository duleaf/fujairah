//
//  RightViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RightViewController : UIViewController
@property (nonatomic,strong) IBOutlet UITableView* tableView;

-(IBAction)menuButtonPressed:(id)sender;

@end
