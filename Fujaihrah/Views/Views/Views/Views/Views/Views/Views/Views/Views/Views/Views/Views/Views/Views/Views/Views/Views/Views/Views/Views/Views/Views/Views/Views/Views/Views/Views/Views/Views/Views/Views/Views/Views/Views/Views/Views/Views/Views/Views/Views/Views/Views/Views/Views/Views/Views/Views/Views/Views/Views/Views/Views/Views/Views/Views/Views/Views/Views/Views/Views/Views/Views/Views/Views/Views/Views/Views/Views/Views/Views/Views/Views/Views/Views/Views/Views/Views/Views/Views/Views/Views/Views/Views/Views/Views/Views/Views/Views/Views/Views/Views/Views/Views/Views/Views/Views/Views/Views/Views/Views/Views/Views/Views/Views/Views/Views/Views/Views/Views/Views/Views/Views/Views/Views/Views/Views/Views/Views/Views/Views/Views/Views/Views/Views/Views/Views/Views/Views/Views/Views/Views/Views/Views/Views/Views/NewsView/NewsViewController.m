//
//  NewsViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "NewsViewController.h"
#import "NewsTableViewCell.h"
@interface NewsViewController ()

@end

@implementation NewsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = @"News";
    self.navigationController.navigationBar.tintColor = [UIColor colorWithRed:189.0/255.0f green:0.0f blue:0.0f alpha:1.0f];
    // Do any additional setup after loading the view from its nib.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    
    // return [[[self fetchedResultsController]fetchedObjects]count];
    
    
    //Acordint to section approches
    //
    //	NSArray *sections = [[self fetchedResultsController] sections];
    //	if (tableView == self.searchDisplayController.searchResultsTableView)
    //	{
    //        return [self.searchResults count];
    //    }else if (sectionIndex < [sections count])
    //	{
    //		id <NSFetchedResultsSectionInfo> sectionInfo = [sections objectAtIndex:sectionIndex];
    //		return sectionInfo.numberOfObjects;
    //	}
	
	return 5;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    
    NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    

    
    if (cell  == nil)
    {
        cell = [[[NSBundle mainBundle] loadNibNamed:@"NewsTableViewCell" owner:nil options:nil] firstObject] ;
        
    }
    
    cell.img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.jpg",(int)indexPath.row % 3]];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
        return  150;
    

}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
