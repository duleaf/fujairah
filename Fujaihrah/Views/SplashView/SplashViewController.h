//
//  SplashViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٧‏/٩‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashViewController : UIViewController

@property (nonatomic,strong) IBOutlet UIImageView * image;
@property (nonatomic,strong) IBOutlet UIImageView * splashImage;
@property (nonatomic,strong) IBOutlet UIButton * ar;
@property (nonatomic,strong) IBOutlet UIButton * en;
@property (strong, nonatomic) JASidePanelController *viewController;


-(IBAction)langSelected:(id)sender;
@end
