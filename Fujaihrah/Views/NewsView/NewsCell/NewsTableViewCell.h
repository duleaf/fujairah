//
//  NewsTableViewCell.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NewsTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UILabel * titel;
@property (nonatomic,strong) IBOutlet UILabel * date;
@property (nonatomic,strong) IBOutlet UIImageView * arrow;
@property (nonatomic,strong) IBOutlet UIImageView * socailImage;


@end
