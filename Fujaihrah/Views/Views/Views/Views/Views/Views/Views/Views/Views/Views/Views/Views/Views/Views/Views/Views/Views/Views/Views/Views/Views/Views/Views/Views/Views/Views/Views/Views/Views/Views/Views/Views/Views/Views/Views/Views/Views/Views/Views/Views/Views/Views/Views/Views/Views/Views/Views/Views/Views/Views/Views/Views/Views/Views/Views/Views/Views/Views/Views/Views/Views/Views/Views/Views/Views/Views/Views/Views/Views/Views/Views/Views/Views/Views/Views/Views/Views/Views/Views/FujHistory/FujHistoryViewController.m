//
//  FujHistoryViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٥‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujHistoryViewController.h"
#import "ASScroll.h"
@interface FujHistoryViewController ()

@end

@implementation FujHistoryViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}


-(ASScroll*) buildPager
{
    ASScroll *asScroll = [[ASScroll alloc]initWithFrame:CGRectMake(0.0,0.0,320.0,200.0)];
    
	// Do any additional setup after loading the view, typically from a nib.
    NSMutableArray * imagesArray = [[NSMutableArray alloc]init];
    int noOfImages = 3 ;
    for (int imageCount = 0; imageCount < noOfImages; imageCount++)
    {
        [imagesArray addObject:[NSString stringWithFormat:@"list%d.png",imageCount+1]];
    }
    [asScroll setArrOfImages:imagesArray];
//    [self.view addSubview:asScroll];
    
    return asScroll;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString * cellIdentifier = [NSString stringWithFormat:@"Cell %i",(int)indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    
    
    if (cell  == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
//    cell.img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.jpg",(int)indexPath.row % 3]];
    
    if(indexPath.row == 0)
    {
        [cell addSubview:[self buildPager]];

    }
    else
    {
        UIView *details ;
        
            if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
                details = [[[NSBundle mainBundle] loadNibNamed:@"HistoryDetails" owner:nil options:nil] firstObject];
        else
            
            details = [[[NSBundle mainBundle] loadNibNamed:@"HistoryDetails" owner:nil options:nil] objectAtIndex:1];
        
        [cell addSubview:details];
    }
    
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == 0)
    {
        return 200;
    }
    else
    {        
        if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
          return   385;
        else
          return  515;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
