//
//  FujAboutUAE.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١١‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujairahObject.h"

@interface FujAboutUAE : FujairahObject


@property (nonatomic, retain) NSString * text_ar;
@property (nonatomic, retain) NSString * text;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) NSString * title_ar;

-(NSArray *)geAboutUAE;

@end
