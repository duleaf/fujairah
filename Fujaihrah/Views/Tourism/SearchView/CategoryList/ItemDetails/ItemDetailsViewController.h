//
//  ItemDetailsViewController.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <EventKit/EventKit.h>
#import <EventKitUI/EventKitUI.h>
#import <MessageUI/MFMailComposeViewController.h>
#import "FujPlace.h"
@interface ItemDetailsViewController : UIViewController<EKEventEditViewDelegate,MFMailComposeViewControllerDelegate>

@property (nonatomic,strong) IBOutlet UIImageView * img;
@property (nonatomic,strong) IBOutlet UIButton * addToPlan;
@property (nonatomic,strong) IBOutlet UIButton * phone;
@property (nonatomic,strong) IBOutlet UIButton * mail;
@property (nonatomic,strong) IBOutlet UIButton * fb;
@property (nonatomic,strong) IBOutlet UIButton * tw;
@property (nonatomic,strong) IBOutlet UIWebView * articalDetails;
@property (nonatomic,strong) IBOutlet UIScrollView * scroll;
@property (nonatomic,strong) FujPlace * place;

-(IBAction) addPlan;
-(IBAction)shareButtonPressed:(id)sender;
-(IBAction)showMap:(id)sender;
-(IBAction)callPhone:(id)sender;

@end
