//
//  FujRulerViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FujairahViewController.h"
@interface FujRulerViewController : FujairahViewController <UIWebViewDelegate>

@property (nonatomic,strong) IBOutlet UITableView * table;
@property (nonatomic,strong) UIScrollView * topImagesScroller;
@property (nonatomic,strong)  IBOutlet UIWebView * webView;


@end
