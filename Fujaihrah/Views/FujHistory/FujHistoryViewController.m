//
//  FujHistoryViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٥‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujHistoryViewController.h"
#import "ASScroll.h"
#import "Utitlities.h"
#import "FujHistory.h"
@interface FujHistoryViewController ()
{
    FujHistory * history ;
}
@end

@implementation FujHistoryViewController

int height;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    height = 200;
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
    
    self.table.backgroundView = [[UIView alloc] initWithFrame:self.table.frame];
    self.table.backgroundColor = [UIColor clearColor];
    
    history = [[FujHistory alloc] init];
    history = [[history getHistory] lastObject];
    
    if(history.text.length < 1)
        [self showLoading];
    
    UIImageView * backgrounImage = [[UIImageView alloc] initWithFrame:self.table.frame];
    [backgrounImage setImage:[UIImage imageNamed:@"baladya_bg.png"]];
    [self.table.backgroundView addSubview:backgrounImage];
    
    if([[LanguageController  getCurrentLanguage] isEqualToString:@"ar"])
    {
        self.title = @"تاريخ الفجيرة";
    }
    else
    {
        self.title = @"Fujairah History";
    }
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(reloadCurrenData:)
     name:NSStringFromClass(self.class)
     object:nil];

    
    // Do any additional setup after loading the view.
}




- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
    return 2;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSString * cellIdentifier = [NSString stringWithFormat:@"Cell %i",(int)indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    
    
    if (cell  == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        
    }
    
//    cell.img.image = [UIImage imageNamed:[NSString stringWithFormat:@"%i.jpg",(int)indexPath.row % 3]];
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    if(indexPath.row == 0)
    {
        [self setupScrollerImager];

        [cell addSubview:self.topImagesScroller];

    }
    else
    {
        UIView *details ;
        
            if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
                details = [[[NSBundle mainBundle] loadNibNamed:@"HistoryDetails" owner:nil options:nil] firstObject];
        else
            
            details = [[[NSBundle mainBundle] loadNibNamed:@"HistoryDetails" owner:nil options:nil] firstObject];
        UILabel *title = (UILabel*)[details viewWithTag:3];
        title.text = history.title;
        if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
            title.text = history.title_ar ;

        title.numberOfLines = 0;
        details.backgroundColor = [UIColor clearColor];
        self.webView = (UIWebView*)[details viewWithTag:5];
        self.webView.delegate = self;
        [self loadWebView];
        
        if(history.text == nil)
            self.webView.hidden = YES;
        
        [cell addSubview:details];
    }
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    if(indexPath.row == 0)
    {
        return 200;
    }
    else
    {        
//        if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
//          return   1056;
//        else
          return  height + self.webView.frame.origin.y;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setupScrollerImager
{
    self.topImagesScroller = [[UIScrollView alloc] initWithFrame:CGRectMake(0, 0, 320, 200)];
    self.topImagesScroller.tag = 1;
//    self.topImagesScroller.delegate = self;
    self.topImagesScroller.pagingEnabled = YES;
    
    
    self.topImagesScroller.contentSize = CGSizeMake(3 * 320, 200);
    NSArray * imagesUrls = [NSArray arrayWithObjects:@"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/FujairahImage-1.png",@"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/FujairahImage-3.png",@"http://fujairah.ae/_LAYOUTS/15/images/Fujairah.SP.Portal/FujairahImage-4.png", nil];
    
    
    for (int i = 0; i < 3; i++)
    {
        UIImageView * image = [[UIImageView alloc] initWithFrame:CGRectMake(i * 320, 0, 320, 200)];
        
        [image sd_setImageWithURL:[NSURL URLWithString:[imagesUrls objectAtIndex:i]] placeholderImage:nil];
        [self.topImagesScroller addSubview:image];
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    //    frame.size.height = 1;
    //    aWebView.frame = frame;
    BOOL flage = (height == 200);
    
    
    height = [[aWebView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue];
    
    if(flage)
    {
        self.webView.frame = CGRectMake(self.webView.frame.origin.x, self.webView.frame.origin.y, self.webView.frame.size.width, height);
        [self.table reloadData];
    }
    
}

-(void)loadWebView
{
    
//@"<p>1. You agree that you will be the technician servicing this work order?.<br>2. You are comfortable with the scope of work on this work order?.<br>3. You understand that if you go to site and fail to do quality repair for  any reason, you will not be paid?.<br>4. You must dress business casual when going on the work order.</p>"    
    NSString * html = [Utitlities getHtmlFromatFor:history.text];
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        html = [Utitlities getHtmlFromatFor:history.text_ar] ;

    
    [self.webView loadHTMLString:html baseURL:nil];
    self.webView.backgroundColor = [UIColor clearColor];
    [self.webView setOpaque:NO];
    NSLog(@"%f",self.webView.scrollView.contentSize.height);
    self.webView.scrollView.scrollEnabled = NO;
}



-(void)reloadCurrenData:(NSNotification*)userInfo
{
//    titles= [cats getAllCategories];
    history = [[FujHistory alloc] init];
    history = [[history getHistory] lastObject];
    height = 200;
    [self loadWebView];
    [self hideLoading];
    [self.table reloadData];
    
}

-(void)showLoading
{
    [super showLoading];
    self.webView.hidden = YES;
}
-(void)hideLoading
{
    [super hideLoading];
    self.webView.hidden = NO;
}


- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
