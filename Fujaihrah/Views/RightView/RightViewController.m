//
//  RightViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "RightViewController.h"
#import "NewsViewController.h"
@interface RightViewController ()

@end

@implementation RightViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
     self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero]; 
}

-(void)viewWillAppear:(BOOL)animated
{
    self.navigationController.navigationBar.hidden = YES;
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 8;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellID = [NSString stringWithFormat:@"cell%i",(int)indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID ];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellID];
    }

    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    UINavigationController * nav =  [self.storyboard instantiateViewControllerWithIdentifier:@"homenav"];
    
    UIViewController * viewC = nil;//
   
    
    if (indexPath.row == 0)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"home"];
    }
    
    else if (indexPath.row == 1)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"fujruler"];
    }
    else if (indexPath.row == 2)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"aboutuae"];
    }
    
    else if (indexPath.row == 3)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"fujhistory"];
    }
    else if (indexPath.row == 4)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"news"];
    }
    else if (indexPath.row == 5)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"govern"];
    }
    
    
    else if (indexPath.row ==6)
    {
        viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"tor"];
    }
    
    else if (indexPath.row == 7)
    {
        if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        {
            viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUsAR"];

        }
        else
            viewC = [self.storyboard instantiateViewControllerWithIdentifier:@"AboutUs"];
    }
    
    if(viewC != nil)
    {
        nav.viewControllers = nil;
        nav.viewControllers = [NSArray arrayWithObject:viewC];
        [self.sidePanelController setCenterPanel:nav];
        [self.sidePanelController showCenterPanelAnimated:YES];
    }
    
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"en"])
    {
        viewC.navigationItem.rightBarButtonItem = nil;
    }
    else
    {
        UIBarButtonItem * barButton;
        UIButton *button =  [UIButton buttonWithType:UIButtonTypeCustom];
        [button setImage:[UIImage imageNamed:@"menu button.png"] forState:UIControlStateNormal];
        [button addTarget:self action:@selector(menuButtonPressed:)forControlEvents:UIControlEventTouchUpInside];
        [button setFrame:CGRectMake(0, 5, 27, 30)];
        
        barButton = [[UIBarButtonItem alloc] initWithCustomView:button];
        viewC.navigationItem.rightBarButtonItem = barButton;
        [barButton setTarget:self];
        [barButton setAction:@selector(menuButtonPressed:)];
    }
    
}


-(IBAction)menuButtonPressed:(id)sender
{
    [self.sidePanelController toggleRightPanel:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
