//
//  NewsDetialsViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "NewsDetialsViewController.h"
#import "Utitlities.h"
@interface NewsDetialsViewController ()

@end

@implementation NewsDetialsViewController

int height;
int startPoint;

- (void)viewDidLoad {
    [super viewDidLoad];
    height =200;

    // Do any additional setup after loading the view.
    [self fillData];
    self.navigationController.navigationBar.tintColor = [UIColor whiteColor];
}

-(void)fillData
{
    
    if(self.artical.imageUrl.length < 1)
    {
        self.image.frame = CGRectZero;
        self.articalTitle.frame = CGRectMake(self.articalTitle.frame.origin.x, 0, self.articalTitle.frame.size.width, self.articalTitle.frame.size.height);
        self.articalTitle.frame =[Utitlities getLableSizeFromText:self.artical.title andLable:self.articalTitle];
        self.date.frame = CGRectMake(20, 0 + self.articalTitle.frame.size.height, 280, self.date.frame.size.height);
         startPoint = self.date.frame.origin.y + self.date.frame.size.height;
        self.articalDetails.frame = CGRectMake(20, startPoint + 10, 280, 20);
        
        
//        self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width, startPoint + self.detials.frame.size.height);
    }
    else
    {
        [self.image sd_setImageWithURL:[NSURL URLWithString:self.artical.imageUrl]];
    }
    
    [self loadWebView];
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"] && [self.artical.articalType isEqualToString:NORMAL_NEWS])
    {
        self.articalTitle.textAlignment = NSTextAlignmentRight;
        self.date.textAlignment = NSTextAlignmentRight;
    }
    
//    [self.articalTitler sizeToFit];
    self.articalTitle.text = self.artical.title;
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"] && [self.artical.articalType isEqualToString:NORMAL_NEWS])
        self.articalTitle.text = self.artical.title_ar ;

    self.date.text = self.artical.date;
//    self.detials.text = self.artical.details;
    
}

- (void)webViewDidFinishLoad:(UIWebView *)aWebView {
    //    frame.size.height = 1;
    //    aWebView.frame = frame;
    BOOL flage = (height == 200);
    
    
    height = [[aWebView stringByEvaluatingJavaScriptFromString:@"document.height"] floatValue];
    
    if(flage)
    {
        self.articalDetails.frame = CGRectMake(self.articalDetails.frame.origin.x, self.articalDetails.frame.origin.y, self.articalDetails.frame.size.width, height);
        
            
        if(self.artical.imageUrl.length < 1)
                self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width, startPoint + height + 100);
        else
            self.scroll.contentSize = CGSizeMake(self.scroll.frame.size.width,218 + self.articalDetails.frame.size.height);
    }
}

-(void)loadWebView
{
    
    //@"<p>1. You agree that you will be the technician servicing this work order?.<br>2. You are comfortable with the scope of work on this work order?.<br>3. You understand that if you go to site and fail to do quality repair for  any reason, you will not be paid?.<br>4. You must dress business casual when going on the work order.</p>"
    NSString * html = [Utitlities getHtmlFromatFor:self.artical.details];
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        html = [Utitlities getHtmlFromatFor:self.artical.details] ;

    
    [self.articalDetails loadHTMLString:html baseURL:nil];
    self.articalDetails.backgroundColor = [UIColor clearColor];
    [self.articalDetails setOpaque:NO];
    NSLog(@"%f",self.articalDetails.scrollView.contentSize.height);
    self.articalDetails.scrollView.scrollEnabled = NO;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
