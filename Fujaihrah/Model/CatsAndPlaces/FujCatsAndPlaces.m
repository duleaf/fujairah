//
//  CatsAndPlaces.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١١‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujCatsAndPlaces.h"
#import "Utitlities.h"
@implementation FujCatsAndPlaces



-(NSMutableArray*)getAllPlacesIdsFromCategory:(PlacesCateogries*)cat
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CatsAndPlaces"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"cat_id == %@",cat.cat_id];
    [fetchRequest setPredicate:predicate];
    NSArray * allProperties = [Utitlities allPropertyNamesofType:CatsAndPlaces.class];
    NSMutableArray * entities = [[NSMutableArray alloc] init];
    
    for(CatsAndPlaces * place in objects)
    {
        FujCatsAndPlaces * fujCatsAndPlace = [[FujCatsAndPlaces alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [fujCatsAndPlace setValue:[place valueForKey:property] forKey:property];
            
        }
        
        [entities addObject:fujCatsAndPlace];
    }
    
    return entities;
}


-(void)insertObjects:(NSArray *)objects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"CatsAndPlaces" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:CatsAndPlaces.class];
    
    
    for(FujCatsAndPlaces * fujCatsAndPlace in objects)
    {
        CatsAndPlaces * place = [NSEntityDescription insertNewObjectForEntityForName:@"CatsAndPlaces" inManagedObjectContext:self.managedObjectContext];
        
        for(NSString * property in allProperties)
        {
            
            [place setValue:[fujCatsAndPlace valueForKey:property] forKey:property];
            
        }
        
        [self.managedObjectContext save:nil];
    }
    
}



-(void)deleteAllObjects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"CatsAndPlaces"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for(NSManagedObject * object in objects)
        [self.managedObjectContext deleteObject:object];
    
}


@end
