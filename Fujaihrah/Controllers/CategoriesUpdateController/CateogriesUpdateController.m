//
//  CaetogriesUpdateController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "CateogriesUpdateController.h"
#import "FujPlacesCateogries.h"
#import "PlacesCateogries.h"
@implementation CateogriesUpdateController


-(void)StartUpdating
{
    self.fujObject = [[FujPlacesCateogries alloc] init];
    
    if([self.fujObject numberOfAvailableRecords] > MIN_NUMBER_OF_ITEMS)
    {
        if ([self isNewUpdateAvailable])
        {
            ;
        }
        [self.delegate UpdateFinished];
    }
    else
    {
        [self.fujObject deleteAllObjects];
        [self fetchAllData];
    }
    
}


-(BOOL) isNewUpdateAvailable
{
    return true;
}

-(void)updateCurrentDataBase
{
    
}

-(void)fetchAllData
{
    NSString * extraUri = @"";
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        extraUri = @"/?lang=ar";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@%@",BASE_URL,self.requestUri,extraUri] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray * categories = [self ParssedObjects:responseObject];
        [self.fujObject insertObjects:categories];
        [self.delegate UpdateFinished];
        //        id obj = [[NSClassFromString(@"class") alloc] init];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


-(NSArray*)ParssedObjects:(id)response
{
    NSDictionary * responseDic = (NSDictionary*) response;
    NSMutableArray * cats = [[NSMutableArray alloc] init];
    NSArray * responsePosts = [responseDic valueForKey:@"categories"];
    
    for(NSDictionary * post in responsePosts)
    {
        FujPlacesCateogries * cat = [[FujPlacesCateogries alloc] init];
        
        cat.name = [post valueForKey:@"name"];
        cat.name_ar = [post valueForKey:@"name_ar"];
        NSNumber * number = [post valueForKey:@"id"];
        cat.cat_id = [NSString stringWithFormat:@"%i",number.intValue];
        
        [cats addObject:cat];
        
    }
    
    return cats;
}

@end
