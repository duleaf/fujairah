//
//  CategoryListTableViewCell.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "CategoryListTableViewCell.h"
#import "Utitlities.h"
@implementation CategoryListTableViewCell

- (void)setData
{
    _title.text = _place.name;
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        _title.text = _place.name_ar ;

    NSString * html = [Utitlities getHtmlFromatFor:_place.desc];
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        html = [Utitlities getHtmlFromatFor:_place.desc_ar] ;
    
    
    [self.webView loadHTMLString:html baseURL:nil];
    self.webView.backgroundColor = [UIColor clearColor];
    [self.webView setOpaque:NO];
    NSLog(@"%f",self.webView.scrollView.contentSize.height);
    self.webView.scrollView.scrollEnabled = NO;
    self.webView.userInteractionEnabled = NO;
    
    _desc.text  = @"";//_place.desc;
    [_image sd_setImageWithURL:[NSURL URLWithString:_place.icon_url]];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
