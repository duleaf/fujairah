//
//  CategoryListViewController.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "CategoryListViewController.h"
#import "FujPlace.h"
#import "CategoryListTableViewCell.h"
#import "Constants.h"
#import "ItemDetails/ItemDetailsViewController.h"
#import "LocationView.h"
@interface CategoryListViewController ()
{
    LocationView * locationView;
    FujPlace *place;
}
@end

@implementation CategoryListViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    
     place = [[FujPlace alloc] init];
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(reloadCurrenData:)
     name:NSStringFromClass(self.class)
     object:nil];
    
    places = [place getPlacesBycategory:self.selectedCategory];
    
//    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
//    {
//        place.name = @"وايفز";
//        place.desc =  @"وصف مفصل لمطعم وايفز";
//        place.lat = @"25.1138498";
//        place.lang = @"55.1629823";
//        
//        [places addObject:place];
//        
//        place = [[FujPlace alloc] init];
//        place.name = @"وايفز١";
//        place.desc =  @"وصف مفصل لمطعم وايفز١";
//        place.lat = @"25.1013367";
//        place.lang = @"55.1603215";
//        
//        [places addObject:place];
//    }
//    else
//    {
//        place.name = @"Waves";
//        place.desc =  @"Waves resturant description is writter here in the place";
//        place.lat = @"25.1138498";
//        place.lang = @"55.1629823";
//        
//        [places addObject:place];
//        
//        place = [[FujPlace alloc] init];
//        place.name = @"Waves1";
//        place.desc =  @"Waves resturant description is writter here in the place";
//        place.lat = @"25.1013367";
//        place.lang = @"55.1603215";
//        
//        [places addObject:place];
//    }

    
    locationView = [[[NSBundle mainBundle] loadNibNamed:@"LocationView" owner:nil options:nil] firstObject];

    locationView.locations = [NSMutableArray arrayWithArray:places];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return places.count + 1;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
     NSString *cellId = [NSString stringWithFormat:@"cell%i",(int)indexPath.row];
    
    
    if (indexPath.row == 0)
    {
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];

        
        if (cell == nil)
        {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellId];
            
            [locationView drawLocations];
        }
        [cell addSubview:locationView];
        return cell;
        
    }
    else
    {
        CategoryListTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellId];

        if (cell == nil)
        {
            NSString * nibFileName =@"CategoryListTableViewCell";
            
            if(!([[LanguageController getCurrentLanguage] isEqualToString:ENGLISH_LANG]))
                nibFileName = [NSString stringWithFormat:@"CategoryListTableViewCell_%@",[LanguageController getCurrentLanguage]];
            
            cell = [[[NSBundle mainBundle] loadNibNamed:nibFileName owner:nil options:nil] firstObject];
        }
        
        NSLog(@"Current Index : %i",(int)indexPath.row -1);
        
        cell.place = [places objectAtIndex:indexPath.row - 1 ];
        [cell setData];
        return cell;
    }
    

    
    // Configure the cell...
    
    
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return  200;
    }
    
    else 
    return 80;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row > 0)
    {
        NSString * nibName = @"ItemDetailsViewController";
        
        if ([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        {
            nibName = @"ItemDetailsViewController_ar";
        }
        
        ItemDetailsViewController *itemDetails = [[ItemDetailsViewController alloc] initWithNibName:nibName bundle:[NSBundle mainBundle]];
        
        itemDetails.place = [places objectAtIndex:indexPath.row - 1 ];
        
        [self.navigationController pushViewController:itemDetails animated:YES];
//        itemDetails
    }
}

-(void)reloadCurrenData:(NSNotification*)userInfo
{
    places = [place getPlacesBycategory:self.selectedCategory];
    [table reloadData];
}



- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
