//
//  ArticalUpdateController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "ArticalUpdateController.h"
#import "FujArtical.h"
@implementation ArticalUpdateController


-(void)StartUpdating
{
    self.fujObject = [[FujArtical alloc] init];
    
    if([self.fujObject numberOfAvailableRecords] > MIN_NUMBER_OF_ITEMS)
    {
        if ([self isNewUpdateAvailable])
        {
            ;
        }
        [self.delegate UpdateFinished];
    }
    else
    {
        if([self.requestUri isEqualToString:@"get_posts"])
        [self.fujObject deleteAllObjects];
        [self fetchAllData];
    }
    
}


-(BOOL) isNewUpdateAvailable
{
    return true;
}

-(void)updateCurrentDataBase
{
    
}

-(void)fetchAllData
{
    NSString * extraUri = @"";
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
        extraUri = @"/?lang=ar";
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    [manager GET:[NSString stringWithFormat:@"%@%@%@",BASE_URL,self.requestUri,extraUri] parameters:nil success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        NSArray * articals ;
        if([self.requestUri isEqualToString:@"get_posts"])
        articals = [self ParssedObjects:responseObject];
        
        else if([self.requestUri isEqualToString:@"get_facebook_feed"])
            articals = [self ParssedObjectsForFB:responseObject];
        
        else if([self.requestUri isEqualToString:@"get_twitter_feed"])
            articals = [self ParssedObjectsForTW:responseObject];
        
        
        [self.fujObject insertObjects:articals];
        [self.delegate UpdateFinished];
//        id obj = [[NSClassFromString(@"class") alloc] init];
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
    }];
}


-(NSArray*)ParssedObjects:(id)response
{
    NSDictionary * responseDic = (NSDictionary*) response;
    NSMutableArray * articals = [[NSMutableArray alloc] init];
    NSArray * responsePosts = [responseDic valueForKey:@"posts"];
    
    for(NSDictionary * post in responsePosts)
    {
        FujArtical * artical = [[FujArtical alloc] init];
        
        artical.title = [post valueForKey:@"title"];
        artical.title_ar = [post valueForKey:@"title_ar"];
        artical.articalType = NORMAL_NEWS;
        artical.details = [post valueForKey:@"content"];
        artical.details_ar = [post valueForKey:@"content_ar"];
        NSNumber * number = [post valueForKey:@"id"];
        artical.art_id = [NSString stringWithFormat:@"%i",number.intValue];
        
        [articals addObject:artical];
        
    }
    
    return articals;
}

-(NSArray *) ParssedObjectsForTW:(id)response
{
    NSDictionary * responseDic = (NSDictionary*) response;
    NSMutableArray * articals = [[NSMutableArray alloc] init];
    NSArray * responsePosts = [responseDic valueForKey:@"items"];
    
    for(NSDictionary * post in responsePosts)
    {
        FujArtical * artical = [[FujArtical alloc] init];
        
        artical.title = [post valueForKey:@"title"];
        artical.details = [post valueForKey:@"description"];
        artical.articalType = TWITTER_NEWS;
        artical.art_id = [post valueForKey:@"link"];
        [articals addObject:artical];
        
    }
    
    return articals;
}

-(NSArray *) ParssedObjectsForFB:(id)response
{
    NSDictionary * responseDic = (NSDictionary*) response;
    NSMutableArray * articals = [[NSMutableArray alloc] init];
    NSArray * responsePosts = [responseDic valueForKey:@"items"];
    
    for(NSDictionary * post in responsePosts)
    {
        FujArtical * artical = [[FujArtical alloc] init];
        
        artical.title = [post valueForKey:@"title"];
        artical.details = [post valueForKey:@"content"];
        artical.date = [post valueForKey:@"published"];
        artical.articalType = FACEBOOK_NEWS;
        NSNumber * number = [post valueForKey:@"id"];
        artical.art_id = [NSString stringWithFormat:@"%i",number.intValue];

        [articals addObject:artical];
        
    }
    
    return articals;
}

@end
