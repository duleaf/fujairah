//
//  UpdateController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FujairahObject.h"
#import "AFNetworking.h"

@protocol UpdateDelegate <NSObject>

-(void)UpdateFinished;

@end

#define MIN_NUMBER_OF_ITEMS 1000000

@interface UpdateController : NSObject

@property (nonatomic,strong) FujairahObject * fujObject;
@property (nonatomic,strong) NSString * requestUri;
@property (nonatomic) id <UpdateDelegate> delegate;

-(void)StartUpdating;


@end
