//
//  NewsDetialsViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FujArtical.h"
@interface NewsDetialsViewController : UIViewController

@property (nonatomic,strong) IBOutlet UILabel *articalTitle;
@property (nonatomic,strong) IBOutlet UILabel *date;
@property (nonatomic,strong) IBOutlet UIWebView * articalDetails;
@property (nonatomic,strong) IBOutlet UIImageView *image;
@property (nonatomic,strong) IBOutlet UIScrollView *scroll;
@property (nonatomic,strong) FujArtical * artical;



@end
