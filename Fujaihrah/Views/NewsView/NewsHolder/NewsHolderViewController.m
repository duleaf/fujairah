//
//  NewsHolderViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٩‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "NewsHolderViewController.h"
#import "NewsTableViewCell.h"
#import "Utitlities.h"
#import "FujArtical.h"
@interface NewsHolderViewController ()
{
    NSMutableArray * cellsHieghts;
    UIActivityIndicatorView * loading;

}
@end

@implementation NewsHolderViewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    cellsHieghts = [[NSMutableArray alloc] init];
    self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)sectionIndex
{
	return self.articals.count;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NewsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
    
    
    if (cell  == nil)
    {
        int viewIndex = 0;
        
        if([[LanguageController getCurrentLanguage] isEqualToString:@"en"])
        {
            viewIndex = 1;
        }
        
        if(![self.type isEqualToString:@"normal"])
        {
            viewIndex += 2;
        }
        
        
        cell = [[[NSBundle mainBundle] loadNibNamed:@"NewsTableViewCell" owner:nil options:nil] objectAtIndex:viewIndex] ;
        
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    NSNumber *height = [cellsHieghts objectAtIndex: indexPath.row];
    
    NSLog(@"H: %i",height.intValue);
    FujArtical * artical = [self.articals objectAtIndex:indexPath.row];

    cell.titel.frame =  [Utitlities getLableSizeFromText:artical.title andLable:cell.titel];
    cell.date.frame = CGRectMake(cell.date.frame.origin.x, cell.titel.frame.origin.y + cell.titel.frame.size.height , cell.date.frame.size.width, 20);
    
    cell.arrow.frame = CGRectMake(cell.arrow.frame.origin.x, (height.intValue - cell.arrow.frame.size.height )/ 2, cell.arrow.frame.size.width, cell.arrow.frame.size.height);
    
    
    
    if( height.intValue > 90)
    {
        cell.socailImage.frame = CGRectMake(cell.socailImage.frame.origin.x,(height.intValue - 70) /2 , cell.socailImage.frame.size.width, 70);
    }
    
    else
    cell.socailImage.frame = CGRectMake(cell.socailImage.frame.origin.x,  height.intValue * 0.2 , cell.socailImage.frame.size.width,height.intValue * 0.6);
    
    artical.date = @"12/11/2014";
    cell.titel.text = artical.title;
    
    if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"] && [artical.articalType isEqualToString:NORMAL_NEWS])
        cell.titel.text = artical.title_ar ;

    cell.date.text = artical.date;
    
    if (![artical.articalType isEqualToString:@"normal"])
    {
        if (![artical.articalType isEqualToString:FACEBOOK_NEWS])
        {
            [cell.socailImage setImage:[UIImage imageNamed:@"facebook.png"]];
        }
        else
        {
            [cell.socailImage setImage:[UIImage imageNamed:@"twitter.png"]];
        }
    }
    
    return cell;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FujArtical * artical = [self.articals objectAtIndex:indexPath.row];
    
    NewsDetialsViewController * detialsView = [self.parentView.storyboard instantiateViewControllerWithIdentifier:@"newDetials"];
    detialsView.artical = artical;
    [self.parentView.navigationController popToRootViewControllerAnimated:NO];
    [self.parentView.navigationController pushViewController:detialsView animated:YES];
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int width = 280;
    FujArtical * artical = [self.articals objectAtIndex:indexPath.row];

    
    if(![artical.articalType isEqualToString:@"normal"])
    {
        width = 235;
    }
    
    UILabel * tempLable = [[UILabel alloc] initWithFrame:CGRectMake(20, 2, width, 58)];
    tempLable.font = [UIFont boldSystemFontOfSize:15.0];
    tempLable.frame = [Utitlities getLableSizeFromText:artical.title andLable:tempLable];
    
    int cellHeight = tempLable.frame.size.height + 30;
    [cellsHieghts addObject:[NSNumber numberWithInt:cellHeight]];
    
    return  cellHeight;
}


-(void) showLoading
{
    loading = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    loading.center = self.view.center;
    
    [loading startAnimating];
    [self.view addSubview:loading];
    
    NSArray * subViews = [self.view subviews];
    
    
}

-(void) hideLoading
{
    [loading stopAnimating];
    [loading removeFromSuperview];
}


@end
