//
//  Place.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface Place : NSManagedObject

@property (nonatomic, retain) NSString * cat_id;
@property (nonatomic, retain) NSString * cover_img_url;
@property (nonatomic, retain) NSString * desc;
@property (nonatomic, retain) NSString * fb_url;
@property (nonatomic, retain) NSString * icon_url;
@property (nonatomic, retain) NSString * lang;
@property (nonatomic, retain) NSString * lat;
@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * p_id;
@property (nonatomic, retain) NSString * phone;
@property (nonatomic, retain) NSString * tw_url;
@property (nonatomic, retain) NSString * web;
@property (nonatomic, retain) NSString * desc_ar;
@property (nonatomic, retain) NSString * name_ar;

@end
