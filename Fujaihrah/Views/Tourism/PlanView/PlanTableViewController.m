//
//  PlanTableViewController.m
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "PlanTableViewController.h"
#import "PlanCell.h"
#import "Constants.h"
#import "FujPlan.h"
#import "LocationView.h"
#import "MDViewController.h"
@interface PlanTableViewController ()

{
    FujPlan * plan;
}

@property (nonatomic, strong) EKEventStore *eventStore;
// Default calendar associated with the above event store
@property (nonatomic, strong) EKCalendar *defaultCalendar;

@property (nonatomic, strong) NSArray *events;

@end

@implementation PlanTableViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    plan = [[FujPlan alloc] init];
    

}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    // Check whether we are authorized to access Calendar
    self.events = [plan getListOFPlans];
    [self.tableView reloadData];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return self.events.count ;
}



 - (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
 {
     PlanCell *cell ;
     
     NSString * nibFileName =@"PlanCell";
         
         if(!([[LanguageController getCurrentLanguage] isEqualToString:ENGLISH_LANG]))
             nibFileName = [NSString stringWithFormat:@"PlanCell_%@",[LanguageController getCurrentLanguage]];
         
         cell = [[[NSBundle mainBundle] loadNibNamed:nibFileName owner:nil options:nil] firstObject];
//     }
 
     FujPlan * event = [self.events objectAtIndex:indexPath.row];
     
     cell.title.text = event.place.name ;
     
     if([[LanguageController getCurrentLanguage] isEqualToString:@"ar"])
         cell.title.text = event.place.name_ar ;
     
     cell.date.text  = [self getDateFormate:event.date];
 // Configure the cell...
 
 return cell;
 }
 

-(NSString*) getDateFormate :(NSDate*)date
{
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:@"yyyy-MMMM-dd"];
    
    NSDateFormatter *timeFormat = [[NSDateFormatter alloc] init];
    [timeFormat setDateFormat:@"HH:mm a"];
    
    NSString * finalFormat = [NSString stringWithFormat:@"%@ , %@",[timeFormat stringFromDate:date],[dateFormat stringFromDate:date]];
    
    
    return finalFormat;
}
#pragma mark Fetch events


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 60;
}

#pragma mark Access Calendar

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{

    MDViewController * mapView = [[MDViewController alloc] init];
    
    FujPlan * selectedPlan = [self.events objectAtIndex:indexPath.row];
    mapView.title = selectedPlan.place.name;
    mapView.place = selectedPlan.place;
    
    [self.navigationController pushViewController:mapView animated:YES];
    
    mapView.navigationItem.leftBarButtonItem.tintColor = [UIColor whiteColor];
}



/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
