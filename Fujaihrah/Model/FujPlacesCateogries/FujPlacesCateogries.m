//
//  FujPlacesCateogries.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujPlacesCateogries.h"
#import "PlacesCateogries.h"
#import "Utitlities.h"

@implementation FujPlacesCateogries



-(void)dealWithBackendResponse:(id)response
{
    
}
-(int)numberOfAvailableRecords
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"PlacesCateogries" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    return (int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
}
-(void) insertObjects:(NSArray*)objects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"PlacesCateogries" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:PlacesCateogries.class];
    
    
    for(FujPlacesCateogries * fujCat in objects)
    {
        PlacesCateogries * cat = [NSEntityDescription insertNewObjectForEntityForName:@"PlacesCateogries" inManagedObjectContext:self.managedObjectContext];
        
        for(NSString * property in allProperties)
        {
            
            
            [cat setValue:[fujCat valueForKey:property] forKey:property];
            
        }
        
        [self.managedObjectContext save:nil];
    }
}

-(NSArray *)getAllCategories
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"PlacesCateogries"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:PlacesCateogries.class];
    NSMutableArray * cats = [[NSMutableArray alloc] init];
    
    for(PlacesCateogries * placeCat in objects)
    {
        FujPlacesCateogries * cat = [[FujPlacesCateogries alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [cat setValue:[placeCat valueForKey:property] forKey:property];
            
        }
        
        [cats addObject:cat];
    }
    
    return [NSArray arrayWithArray:cats];
}



-(void)deleteAllObjects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"PlacesCateogries"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for(NSManagedObject * object in objects)
        [self.managedObjectContext deleteObject:object];
    
}


@end
