//
//  ViewController.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٤‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FujPlace.h"
#import "FujairahViewController.h"
@interface SearchViewController : FujairahViewController <UITableViewDelegate,UITableViewDataSource,UITextFieldDelegate>

@property (nonatomic,strong) IBOutlet UITableView * tableView;
@property (nonatomic,strong) IBOutlet UITextField * searchPlace;

@end
