//
//  NewsViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٣١‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MHTabBarController.h"
@interface NewsViewController : MHTabBarController

@property (nonatomic,strong) IBOutlet UITableView * table;

-(void)reloadCurrenData:(NSNotification*)userInfo;
@end
