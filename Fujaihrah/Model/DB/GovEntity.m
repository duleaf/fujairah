//
//  GovEntity.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "GovEntity.h"


@implementation GovEntity

@dynamic ent_id;
@dynamic fb;
@dynamic image;
@dynamic intro;
@dynamic lang;
@dynamic lat;
@dynamic logo;
@dynamic name;
@dynamic phone;
@dynamic targets;
@dynamic tw;
@dynamic webSite;
@dynamic intro_ar;
@dynamic name_ar;

@end
