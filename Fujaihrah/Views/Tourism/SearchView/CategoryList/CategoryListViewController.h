//
//  CategoryListViewController.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FujPlacesCateogries.h"
#import "FujairahViewController.h"
@interface CategoryListViewController : FujairahViewController<UITableViewDataSource,UITableViewDelegate>

{
    IBOutlet UITableView *table;
    NSMutableArray * places;
}

@property (nonatomic,strong) FujPlacesCateogries *selectedCategory;


@end
