//
//  AboutUsViewController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "AboutUsViewController.h"

@interface AboutUsViewController ()

@end

@implementation AboutUsViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.table.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    if([[LanguageController  getCurrentLanguage] isEqualToString:@"ar"])
    {
        self.title = @"اتصل بنا";
    }
    else
    {
        self.title = @"Call Us";
    }
}



#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
    
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 6;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *CellID = [NSString stringWithFormat:@"cell%i",(int)indexPath.row];
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellID ];
    
    if(cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellID];
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == 0)
    {
        UIDevice *device = [UIDevice currentDevice];
        if ([[device model] isEqualToString:@"iPhone"] ) {
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:[NSString stringWithFormat:@"telprompt://%@",@"0097192222111"]]];
        } else {
            UIAlertView *notPermitted=[[UIAlertView alloc] initWithTitle:@"Alert" message:@"Your device doesn't support this feature." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [notPermitted show];
        }
    }
    else if (indexPath.row == 1)
    {
        MFMailComposeViewController* controller = [[MFMailComposeViewController alloc] init];
        controller.mailComposeDelegate = self;
        [controller setToRecipients:[NSArray arrayWithObject:@"egov@fujairah.ae"]];
        if (controller)
            [self presentViewController:controller animated:YES completion: nil];
        
    }
    else if (indexPath.row == 2)
    {
        
    }
    else if (indexPath.row == 3)
    {
        NSString * url = @"https://www.facebook.com/109138242438583";
        NSURL * nsurl = [NSURL URLWithString:[NSString stringWithFormat:@"fb://profile/%@",[url lastPathComponent]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:nsurl]){
            [[UIApplication sharedApplication] openURL:nsurl];
        }
        else {
            NSURL *link = [NSURL URLWithString:url];
            [[UIApplication sharedApplication] openURL:link];
        }
        
    }
    else if (indexPath.row == 4)
    {
        NSString * url = @"twitter.com/Fujairah";
        
        NSURL * nsurl = [NSURL URLWithString:[NSString stringWithFormat:@"twitter://user?screen_name=%@",[url lastPathComponent]]];
        
        if ([[UIApplication sharedApplication] canOpenURL:nsurl]){
            [[UIApplication sharedApplication] openURL:nsurl];
        }
        else {
            NSURL *link = [NSURL URLWithString:url];
            [[UIApplication sharedApplication] openURL:link];
        }
        
    }
    else if (indexPath.row == 5)
    {
        NSString * url = @"http://fujairah.ae";
        
        NSURL *link = [NSURL URLWithString:url];
        [[UIApplication sharedApplication] openURL:link];
    }
    
}

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
