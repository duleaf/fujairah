//
//  FujAboutUAE.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١١‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "FujAboutUAE.h"
#import "AboutUAE.h"
#import "Utitlities.h"
@implementation FujAboutUAE

-(int)numberOfAvailableRecords
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"AboutUAE" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSLog(@"Count: %i",(int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil]);
    
    return (int)[self.managedObjectContext countForFetchRequest:fetchRequest error:nil];
}


-(void)insertObjects:(NSArray *)objects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription
                                   entityForName:@"AboutUAE" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:AboutUAE.class];
    
    
    for(FujAboutUAE * fujAbout in objects)
    {
        AboutUAE * about = [NSEntityDescription insertNewObjectForEntityForName:@"AboutUAE" inManagedObjectContext:self.managedObjectContext];
        
        for(NSString * property in allProperties)
        {
            
            
            [about setValue:[fujAbout valueForKey:property] forKey:property];
            
        }
        
        [self.managedObjectContext save:nil];
    }
    
}

-(NSArray *)geAboutUAE
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"AboutUAE"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    NSArray * allProperties = [Utitlities allPropertyNamesofType:AboutUAE.class];
    NSMutableArray * articals = [[NSMutableArray alloc] init];
    
    for(AboutUAE * about in objects)
    {
        FujAboutUAE * fujAbout = [[FujAboutUAE alloc] init];
        
        for(NSString * property in allProperties)
        {
            
            [fujAbout setValue:[about valueForKey:property] forKey:property];
            
        }
        
        [articals addObject:fujAbout];
    }
    
    return [NSArray arrayWithArray:articals];
}

-(void)deleteAllObjects
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"AboutUAE"];
    NSArray *objects = [[self.managedObjectContext executeFetchRequest:fetchRequest error:nil] mutableCopy];
    
    for(NSManagedObject * object in objects)
        [self.managedObjectContext deleteObject:object];
    
}

@end
