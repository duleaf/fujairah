//
//  CategoryListTableViewCell.h
//  FUJ1
//
//  Created by Mohammed Salah on ١٧‏/٨‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FujPlace.h"
@interface CategoryListTableViewCell : UITableViewCell

@property(nonatomic,strong) IBOutlet UIImageView * image;
@property(nonatomic,strong) IBOutlet UILabel * title;
@property(nonatomic,strong) IBOutlet UILabel * desc;
@property(nonatomic,strong) IBOutlet UIWebView * webView;
@property(nonatomic,strong)  FujPlace * place;

-(void)setData;
@end
