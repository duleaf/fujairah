
//
//  LanguageController.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٥‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "LanguageController.h"

@implementation LanguageController

static NSString * currentLanguage;

+(void)setCurrentLanguage:(NSString*)lang
{
    currentLanguage = lang;
}

+(NSString*) getCurrentLanguage
{
    return currentLanguage;
}

+(NSString*) localizedStringForKey:(NSString*)key
{
    NSString * currentLang = [LanguageController getCurrentLanguage];
    
    NSString *path= [[NSBundle mainBundle] pathForResource:currentLang ofType:@"lproj"];
    NSBundle* languageBundle = [NSBundle bundleWithPath:path];
    //    NSLocalizedStringFromTableInBundle(key, nil, languageBundle, nil);
    NSString* str=[languageBundle localizedStringForKey:key value:@"" table:nil];
    
    return str;
}

@end
