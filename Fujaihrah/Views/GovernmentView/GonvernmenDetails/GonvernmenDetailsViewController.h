//
//  GonvernmenDetailsViewController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٥‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FujGovEntityDetails.h"


@interface GonvernmenDetailsViewController : UIViewController<UIWebViewDelegate>


@property (nonatomic,strong) FujGovEntityDetails * entity;
@property (nonatomic,strong) IBOutlet UITableView * table ;
@property (nonatomic,strong) IBOutlet UIWebView * webView;


-(IBAction)callPhone:(id)sender;
-(IBAction)openLink:(id)sender;
-(IBAction)showMap:(id)sender;

@end
