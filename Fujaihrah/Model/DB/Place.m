//
//  Place.m
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "Place.h"


@implementation Place

@dynamic cat_id;
@dynamic cover_img_url;
@dynamic desc;
@dynamic fb_url;
@dynamic icon_url;
@dynamic lang;
@dynamic lat;
@dynamic name;
@dynamic p_id;
@dynamic phone;
@dynamic tw_url;
@dynamic web;
@dynamic desc_ar;
@dynamic name_ar;

@end
