//
//  ArticalUpdateController.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ٦‏/١١‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import "UpdateController.h"

@interface ArticalUpdateController : UpdateController

@property (nonatomic,strong) NSString * articalType;

@end
