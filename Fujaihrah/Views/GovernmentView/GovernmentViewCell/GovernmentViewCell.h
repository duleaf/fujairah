//
//  GovernmentViewCell.h
//  Fujaihrah
//
//  Created by Mohammed Salah on ١٥‏/١٠‏/٢٠١٤.
//  Copyright (c) ٢٠١٤ Mohammed Salah. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GovernmentViewCell : UICollectionViewCell

@property (nonatomic,strong) IBOutlet UIImageView * image;
@property (nonatomic,strong) IBOutlet UILabel * title;


@end
